import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poti_taxi/cubit/driver/cubit/cancel_reason/cubit/cancel_reason_cubit.dart';
import 'package:poti_taxi/cubit/main/cubit/main_cubit.dart';
import 'package:poti_taxi/cubit/user/cart/cubit/cart_cubit.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/cubit/user/dish_category/cubit/dish_category_cubit.dart';
import 'package:poti_taxi/cubit/user/order/cubit/order_cubit.dart';
import 'package:poti_taxi/cubit/user/organization_category/cubit/organization_category_cubit.dart';
import 'package:poti_taxi/cubit/user/organizations/cubit/organization_cubit.dart';
import 'package:poti_taxi/data/repository.dart';
import 'package:poti_taxi/presentations/components/items/organization.dart';
import 'package:poti_taxi/presentations/payment/index.dart';
import 'package:poti_taxi/presentations/screens/auth/client/forget.dart';
import 'package:poti_taxi/presentations/screens/auth/client/login.dart';
import 'package:poti_taxi/presentations/screens/auth/client/register.dart';
import 'package:poti_taxi/presentations/screens/auth/driver/final.dart';
import 'package:poti_taxi/presentations/screens/auth/driver/registration.dart';
import 'package:poti_taxi/presentations/screens/greetings.dart';
import 'package:poti_taxi/presentations/screens/home/driver/block.dart';
import 'package:poti_taxi/presentations/screens/home/driver/index.dart';
import 'package:poti_taxi/presentations/screens/home/driver/line_choose.dart';
import 'package:poti_taxi/presentations/screens/home/driver/map/index.dart';
import 'package:poti_taxi/presentations/screens/home/driver/order/index.dart';
import 'package:poti_taxi/presentations/screens/home/index.dart';
import 'package:poti_taxi/presentations/screens/home/user/order/index.dart';
import 'package:poti_taxi/presentations/screens/home/user/restaurants/organizations/create.dart';
import 'package:poti_taxi/presentations/screens/home/user/restaurants/organizations/index.dart';
import 'package:poti_taxi/presentations/screens/home/user/restaurants/organizations/show.dart';
import 'package:poti_taxi/presentations/screens/start.dart';
import 'package:poti_taxi/service/network_service.dart';

import 'cubit/driver/cubit/driver_cubit.dart';
import 'cubit/driver/cubit/driver_order_cubit.dart';
import 'cubit/driver/cubit/subscription/cubit/subscription_cubit.dart';

class AppRouter {
  Repository repository;

  AppRouter() {
    repository = Repository(networkService: NetworkService());
  }

  Route generateRouter(RouteSettings settings) {
    final dynamic arguments = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => MainCubit(repository: repository)),
                  ],
                  child: StartScreen(),
                ));
      case '/forget':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => MainCubit(repository: repository)),
                  ],
                  child: ForgetScreen(),
                ));
      case '/start':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => MainCubit(repository: repository)),
                ], child: Greetings()));
      case '/register/client':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => OrderCubit(repository: repository))
                ], child: RegisterClient()));
      case '/register/driver':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => DriverCubit(repository: repository)),
                ], child: DriverRegistration()));
      case '/register/driver/final':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => UserCubit(repository: repository),
                  child: FinalRegistration(),
                ));
      case '/home/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              OrderCubit(repository: repository)),
                      BlocProvider(
                          create: (context) =>
                              UserCubit(repository: repository)),
                      BlocProvider(
                          create: (context) =>
                              OrganizationCubit(repository: repository)),
                    ],
                    child: HomeIndex(
                      order: arguments["order"],
                      locationData: arguments['location'],
                    )));
      case '/driver/index':
        return MaterialPageRoute(builder: (context) => DriverIndex());
      case '/driver/choose':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          SubscriptionCubit(repository: repository))
                ], child: LineChoose()));
      case '/driver/map':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          DriverOrderCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          CancelReasonCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                ], child: DriverMap()));
      case '/block':
        return MaterialPageRoute(builder: (context) => BlockPage());
      case '/payment':
        return MaterialPageRoute(
            builder: (context) => PaymentWeb(
                  url: arguments,
                ));
      case '/driver/history':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (BuildContext context) =>
                    DriverOrderCubit(repository: repository),
                child: OrderHistory()));
      case '/login':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (BuildContext context) =>
                      UserCubit(repository: repository),
                  child: Login(
                    driver: arguments,
                  ),
                ));
      case '/user/history':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (BuildContext context) =>
                      OrderCubit(repository: repository),
                  child: UserOrderHistory(),
                ));
      case '/organizations':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            OrganizationCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            OrganizationCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: OrganizationsIndex(
                    point: arguments,
                  ),
                ));
      case '/organization':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            DishCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: OrganizationShow(
                    data: arguments,
                  ),
                ));
      case '/organization/store':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              CartCubit(repository: repository))
                    ],
                    child: OrganizationOrderStore(
                      data: arguments,
                    )));
      default:
        return null;
    }
  }
}
