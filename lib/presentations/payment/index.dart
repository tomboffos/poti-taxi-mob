import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:location/location.dart';

class PaymentWeb extends StatefulWidget {
  final String url;
  PaymentWeb({Key key, this.url}) : super(key: key);

  @override
  _PaymentWebState createState() => _PaymentWebState();
}

class _PaymentWebState extends State<PaymentWeb> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  bool payed = false;
  @override
  void initState() {
    super.initState();
    addUrlListener();
  }

  addUrlListener() async {
    final location = new Location();
    LocationData userData = await location.getLocation();
    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      if (url.contains('subscription/success')) {
        setState(() {
          payed = true;
        });
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map', (route) => false,
            arguments: {'location': userData});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return payed
        ? SizedBox.shrink()
        : WebviewScaffold(
            url: widget.url,
            appBar: AppBar(
              title: Text('Оплата',
                  style: GoogleFonts.openSans(color: Colors.white)),
              backgroundColor: Colors.black,
            ),
          );
  }
}
