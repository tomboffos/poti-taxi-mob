import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/cubit/user/order/cubit/order_cubit.dart';

class UserOrderHistory extends StatelessWidget {
  const UserOrderHistory({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<OrderCubit>(context).getOrders();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('История заказов'),
      ),
      body: BlocBuilder<OrderCubit, OrderState>(
        builder: (context, state) {
          if (!(state is OrdersFetched))
            return Center(
              child: CircularProgressIndicator(),
            );
          final orders = (state as OrdersFetched).orders;
          return ListView.builder(
            itemBuilder: (BuildContext context, index) => Container(
              decoration: BoxDecoration(
                  border: Border.symmetric(
                      horizontal: BorderSide(color: Colors.black))),
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Заказ номер ${orders[index].id}'),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                          'Водитель : ${orders[index].driver != null ? orders[index].driver.user.name : ''}')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          'Цена заказа : ${orders[index].price != null ? orders[index].price : orders[index].area.price} тг'),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            itemCount: orders.length,
          );
        },
      ),
    );
  }
}
