import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cart/cubit/cart_cubit.dart';
import 'package:poti_taxi/data/models/organization.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:poti_taxi/presentations/components/items/cart_item.dart';
import 'package:poti_taxi/presentations/components/items/dish.dart';

class OrganizationOrderStore extends StatefulWidget {
  final Map<String, dynamic> data;
  const OrganizationOrderStore({Key key, this.data}) : super(key: key);

  @override
  _OrganizationOrderStoreState createState() => _OrganizationOrderStoreState();
}

class _OrganizationOrderStoreState extends State<OrganizationOrderStore> {
  void openSuccessDialog({context}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              backgroundColor: Colors.black,
              content: Container(
                height: MediaQuery.of(context).size.height * 0.4,
                child: Column(
                  children: [
                    Container(
                      height: 159,
                      width: 159,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Icon(
                        Icons.check,
                        size: 100,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text('Ваш заказ принят!',
                        style: GoogleFonts.montserrat(
                            fontSize: 20,
                            fontWeight: FontWeight.normal,
                            color: Colors.white)),
                    SizedBox(height: 50),
                    MainButton(
                      splashColor: Colors.black,
                      backgroundColor: Colors.white,
                      borderColor: Colors.white,
                      minWidth: MediaQuery.of(context).size.width * 0.5,
                      borderWidth: 1,
                      borderRadius: 5,
                      action: () => Navigator.pop(context),
                      padding: EdgeInsets.symmetric(vertical: 15),
                      text: Text(
                        'Хорошо',
                        style: GoogleFonts.montserrat(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void openErrorDialog({context}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              backgroundColor: Colors.black,
              content: Container(
                height: MediaQuery.of(context).size.height * 0.4,
                child: Column(
                  children: [
                    Container(
                      height: 159,
                      width: 159,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: Icon(
                        Icons.close,
                        size: 100,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text('Ваш заказ был отклонён!',
                        style: GoogleFonts.montserrat(
                            fontSize: 20,
                            fontWeight: FontWeight.normal,
                            color: Colors.white)),
                    SizedBox(
                      height: 10,
                    ),
                    Text('К сожалению в данное время такой позиции нет.',
                        style: GoogleFonts.montserrat(
                          fontSize: 15,
                          fontWeight: FontWeight.normal,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center),
                    SizedBox(height: 50),
                    MainButton(
                      splashColor: Colors.black,
                      backgroundColor: Colors.white,
                      borderColor: Colors.white,
                      minWidth: MediaQuery.of(context).size.width * 0.5,
                      borderWidth: 1,
                      borderRadius: 5,
                      action: () => Navigator.pop(context),
                      padding: EdgeInsets.symmetric(vertical: 15),
                      text: Text(
                        'Хорошо',
                        style: GoogleFonts.montserrat(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CartCubit>(context)
        .getCart(organization: widget.data['organization']);
    return Scaffold(
      appBar: AppBar(
        actions: [],
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          constraints:
              BoxConstraints(minHeight: MediaQuery.of(context).size.height),
          margin: EdgeInsets.only(left: 24, right: 24, top: 40),
          child: Column(
            children: [
              BlocBuilder<CartCubit, CartState>(
                builder: (context, state) {
                  if (!(state is CartFetched))
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  final cart = (state as CartFetched).cart;
                  return ListView.builder(
                    itemBuilder: (context, index) => CartItem(
                      dish: cart[index],
                      organization: widget.data['organization'],
                    ),
                    itemCount: cart.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                  );
                },
              ),
              SizedBox(
                height: 24,
              ),
              Text('Просим еще раз проверить \n заказанную вами еду!',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff272727).withOpacity(0.5))),
              SizedBox(
                height: 25,
              ),
              TextField(
                maxLines: 3,
                decoration: InputDecoration(
                    hintText: 'Комментарий ресторану...',
                    hintStyle: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff272727).withOpacity(0.5)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color(0xff272727),
                        ),
                        borderRadius: BorderRadius.circular(5)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Color(0xff272727),
                        ),
                        borderRadius: BorderRadius.circular(5))),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 100),
                child: MainButton(
                  splashColor: Colors.white,
                  backgroundColor: Color(0xff272727),
                  borderColor: Color(0xff272727),
                  minWidth: MediaQuery.of(context).size.width * 0.5,
                  borderWidth: 1,
                  borderRadius: 5,
                  action: () => BlocProvider.of<CartCubit>(context).storeOrder(
                      context: context,
                      form: {
                        'organization_id':
                            widget.data['organization'].id.toString(),
                        'point': widget.data['point'],
                      },
                      organization: widget.data['organization']),
                  padding: EdgeInsets.symmetric(vertical: 15),
                  text: Text(
                    'Оформить заказ',
                    style: GoogleFonts.montserrat(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
