import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cart/cubit/cart_cubit.dart';
import 'package:poti_taxi/cubit/user/dish_category/cubit/dish_category_cubit.dart';
import 'package:poti_taxi/data/models/organization.dart';
import 'package:poti_taxi/presentations/components/items/dish_category.dart';

class OrganizationShow extends StatelessWidget {
  final Map<String, dynamic> data;
  const OrganizationShow({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<DishCategoryCubit>(context)
        .getCategories(organizationId: data['organization'].id);
    BlocProvider.of<CartCubit>(context)
        .getCart(organization: data['organization']);
    return Scaffold(
      appBar: AppBar(
        actions: [],
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 24, right: 24, top: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('${data['organization'].name}',
                  style: GoogleFonts.montserrat(
                      fontSize: 16, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 15,
              ),
              Text(
                '${data['organization'].shortDescription}',
                style: GoogleFonts.montserrat(fontSize: 13),
              ),
              SizedBox(
                height: 13,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 16),
                    width: (MediaQuery.of(context).size.width - 48) * 0.6,
                    height: 198,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: NetworkImage('${data['organization'].image}'),
                          fit: BoxFit.cover),
                    ),
                  ),
                  Container(
                    child: Flexible(
                        child: Text('${data['organization'].description}',
                            style: GoogleFonts.montserrat(
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff272727).withOpacity(0.5)))),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              BlocBuilder<DishCategoryCubit, DishCategoryState>(
                builder: (context, state) {
                  if (!(state is DishCategoryFetched))
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  final categories = (state as DishCategoryFetched).categories;
                  return BlocBuilder<CartCubit, CartState>(
                    builder: (context, state) {
                      if (!(state is CartFetched))
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      final cart = (state as CartFetched).cart;
                      return ListView.builder(
                        itemBuilder: (BuildContext context, index) =>
                            DishCategoryItem(
                                category: categories[index],
                                organization: data['organization'],
                                cart: cart),
                        itemCount: categories.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      );
                    },
                  );
                },
              ),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
      floatingActionButton: Container(
        width: 200,
        height: 50,
        margin: EdgeInsets.only(bottom: 40),
        child: FloatingActionButton(
            backgroundColor: Colors.black,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            onPressed: () => Navigator.pushNamed(context, '/organization/store',
                    arguments: data)
                .then((value) => BlocProvider.of<CartCubit>(context)
                    .getCart(organization: data['organization'])),
            child: Text('Оформить заказ',
                style: GoogleFonts.montserrat(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: Colors.white))),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
