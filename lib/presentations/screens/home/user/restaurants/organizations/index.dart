import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/organization_category/cubit/organization_category_cubit.dart';
import 'package:poti_taxi/cubit/user/organizations/cubit/organization_cubit.dart';
import 'package:poti_taxi/presentations/components/form.dart';
import 'package:poti_taxi/presentations/components/items/organization.dart';
import 'package:poti_taxi/presentations/components/items/organization_type.dart';

class OrganizationsIndex extends StatelessWidget {
  final point;
  const OrganizationsIndex({Key key, this.point}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<OrganizationCubit>(context)
        .getOrganizations(point: '[${point[0]},${point[1]}]');
    BlocProvider.of<OrganizationCategoryCubit>(context)
        .getOrganizationCategory();
    return Scaffold(
      appBar: AppBar(
        actions: [],
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 24, right: 24, top: 40),
          child: Column(
            children: [
              BlocBuilder<OrganizationCategoryCubit, OrganizationCategoryState>(
                builder: (context, state) {
                  if (!(state is OrganizationCategoryFetched))
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  final categories =
                      (state as OrganizationCategoryFetched).categories;
                  return Wrap(
                      spacing: 8.0,
                      direction: Axis.horizontal,
                      children: categories
                          .map((e) => OrganizationType(
                                organizationCategory: e,
                              ))
                          .toList());
                },
              ),
              MainTextField(
                  hintText: 'Поиск заведения',
                  onChange: (value) =>
                      BlocProvider.of<OrganizationCubit>(context)
                          .getOrganizations(
                              point: '[${point[0]},${point[1]}]',
                              query: value)),
              SizedBox(height: 50),
              Row(
                children: [
                  Text('Заведения',
                      style: GoogleFonts.montserrat(
                          fontSize: 16, fontWeight: FontWeight.w700))
                ],
              ),
              SizedBox(
                height: 15,
              ),
              BlocBuilder<OrganizationCubit, OrganizationState>(
                builder: (context, state) {
                  if (!(state is OrganizationFetched))
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  final organizations =
                      (state as OrganizationFetched).organizations;
                  return GridView.count(
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    crossAxisSpacing: 16.0,
                    childAspectRatio: 2 / 2,
                    children: List.generate(
                        organizations.length,
                        (index) => OrganizationItem(
                            point: jsonEncode([point[0], point[1]]),
                            organization: organizations[index])),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
