import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animarker/core/ripple_marker.dart';
import 'package:flutter_animarker/widgets/animarker.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as GMap;
import 'package:here_sdk/core.dart' as Geo;
import 'package:here_sdk/mapview.dart';
import 'package:location/location.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/cubit/user/order/cubit/order_cubit.dart';
import 'package:poti_taxi/cubit/user/organizations/cubit/organization_cubit.dart';
import 'package:poti_taxi/data/models/order.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/options.dart';
import 'package:adhara_socket_io/socket.dart';
import 'package:poti_taxi/service/map_service.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart' as YMap;

class HomeIndex extends StatefulWidget {
  final LocationData locationData;
  final Order order;
  const HomeIndex({Key key, this.order, this.locationData}) : super(key: key);

  @override
  _HomeIndexState createState() => _HomeIndexState();
}

class _HomeIndexState extends State<HomeIndex> {
  YandexMapController controller;

  CameraPosition position = new CameraPosition(
      target: LatLng(43.258145717767256, 76.90407988064311), zoom: 14.242);
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Completer<GoogleMapController> _controller = Completer();
  Location location = new Location();
  LatLng coordinates;
  LocationData userLocation;
  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  bool search = false;
  SocketIO socket;

  getLocation() async {
    final GoogleMapController controller = await _controller.future;
    LocationData currentLocation;
    var _location = new Location();

    _location.onLocationChanged.listen((l) {
      print(l);
      setState(() {
        latitude = l.latitude;
        longitude = l.longitude;
        print(latitude);
      });
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15),
        ),
      );
    });
  }

  dialogCancel(context, {action}) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              height: MediaQuery.of(context).size.height / 8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                      child: Text(
                    'Вы точно хотите отменить \n заказ?',
                    style: GoogleFonts.montserrat(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                    textAlign: TextAlign.center,
                  )),
                  SizedBox(
                    height: 11,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MainButton(
                        splashColor: Colors.white,
                        backgroundColor: Colors.transparent,
                        borderColor: Color(0xff272727),
                        minWidth: MediaQuery.of(context).size.width / 4,
                        borderWidth: 1,
                        borderRadius: 5,
                        action: action,
                        padding: EdgeInsets.symmetric(vertical: 15),
                        text: Text(
                          'Да',
                          style: GoogleFonts.montserrat(
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                        ),
                      ),
                      MainButton(
                        splashColor: Colors.white,
                        backgroundColor: Color(0xff272727),
                        borderColor: Color(0xff272727),
                        minWidth: MediaQuery.of(context).size.width / 4,
                        borderWidth: 1,
                        borderRadius: 5,
                        action: () => Navigator.of(context).pop(),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        text: Text(
                          'Нет',
                          style: GoogleFonts.montserrat(
                              fontSize: 15,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
          );
        });
  }

  var latitude;
  var longitude;
  void _currentLocation() async {
    // controller.move(
    //     point: Point(latitude: latitude, longitude: longitude), zoom: 17.0);
    // controller.addPlacemark(Placemark(
    //   point: Point(latitude: latitude, longitude: longitude),
    //   style: PlacemarkStyle(
    //       opacity: 1, scale: 1.0, iconName: 'assets/images/marker.png'),
    // ));
    // final GoogleMapController controller = await _controller.future;
    // LocationData currentLocation;

    // currentLocation = await location.getLocation();
    // print(currentLocation);
    // setState(() {
    //   longitude = currentLocation.longitude;
    //   latitude = currentLocation.latitude;
    // });
    // final markerId = MarkerId('order');

    // var marker = Marker(
    //   markerId: markerId,
    //   position: LatLng(currentLocation.latitude, currentLocation.longitude),
    //   infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
    // );
    // setState(() {
    //   markers.removeWhere((key, value) => key == markerId);
    //   markers[markerId] = marker;
    // });
    // print(marker);
    // controller.animateCamera(CameraUpdate.newCameraPosition(
    //   CameraPosition(
    //     bearing: 1,
    //     target: LatLng(currentLocation.latitude, currentLocation.longitude),
    //     zoom: 17.0,
    //   ),
    // ));
    LocationData currentLocation;
    currentLocation = await location.getLocation();
    setState(() {
      longitude = currentLocation.longitude;
      latitude = currentLocation.latitude;
    });

    mapController.camera
        .lookAtPointWithDistance(Geo.GeoCoordinates(latitude, longitude), 8000);
    mapController.unpinWidget(
      Image.asset(
        'assets/images/marker.png',
        height: 30,
        width: 30,
      ),
    );
    mapController.pinWidget(
      Image.asset(
        'assets/images/marker.png',
        height: 30,
        width: 30,
      ),
      Geo.GeoCoordinates(
          widget.locationData.latitude, widget.locationData.longitude),
    );
  }

  checkOrder({HereMapController controller}) async {
    if (widget.order != null)
      BlocProvider.of<OrderCubit>(context).continueOrder(
        controller: mapController,
        order: widget.order,
      );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _currentLocation();
    // getLocation();

    latitude = widget.locationData.latitude;
    longitude = widget.locationData.longitude;
  }

  HereMapController mapController;
  void _onMapCreated(HereMapController hereMapController) {
    setState(() {
      mapController = hereMapController;
    });
    hereMapController.mapScene.loadSceneForMapScheme(MapScheme.normalDay,
        (MapError error) {
      if (error != null) {
        print('Map scene not loaded. MapError: ${error.toString()}');
        return;
      }
    });
    BlocProvider.of<OrganizationCubit>(context).getOrganizationsMap(
        point:
            '[${widget.locationData.latitude},${widget.locationData.longitude}]',
        controller: mapController);
    double distanceToEarthInMeters = 2000;
    hereMapController.camera.lookAtPointWithDistance(
        Geo.GeoCoordinates(
            widget.locationData.latitude, widget.locationData.longitude),
        distanceToEarthInMeters);
    hereMapController.pinWidget(
      Image.asset(
        'assets/images/marker.png',
        height: 30,
        width: 30,
      ),
      Geo.GeoCoordinates(
          widget.locationData.latitude, widget.locationData.longitude),
    );
    checkOrder(controller: hereMapController);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      endDrawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              onTap: () => Navigator.pushNamed(context, '/user/history'),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.history,
                size: 20,
              ),
              title: Text(
                "История заказов",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
            ListTile(
              onTap: () => Navigator.pushNamed(context, '/organizations',
                  arguments: [latitude, longitude]),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.fastfood,
                size: 20,
              ),
              title: Text(
                "Заказать еду",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
            ListTile(
              onTap: () => launch(
                  'https://api.whatsapp.com/send?phone=77017893383&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5'),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.help_outline,
                size: 20,
              ),
              title: Text(
                "Служба поддержки",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
            // ListTile(
            //   minLeadingWidth: 0,
            //   leading: Icon(
            //     Icons.phone_android,
            //     size: 20,
            //   ),
            //   title: Text(
            //     "Сменить номер телефона",
            //     style: GoogleFonts.montserrat(
            //         fontSize: 13,
            //         fontWeight: FontWeight.w400,
            //         color: Color(0xff272727)),
            //   ),
            // ),
            ListTile(
              onTap: () =>
                  BlocProvider.of<UserCubit>(context).logout(context: context),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.logout,
                size: 20,
              ),
              title: Text(
                "Выход",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          // YandexMap(
          //     onMapCreated: (YandexMapController yandexMapController) async {
          //   controller = yandexMapController;
          //   checkOrder(controller: controller);
          //   print(controller);
          //   controller.move(
          //       point: Point(latitude: latitude, longitude: longitude),
          //       zoom: 17.0);
          //   controller.addPlacemark(Placemark(
          //     point: Point(latitude: latitude, longitude: longitude),
          //     style: PlacemarkStyle(
          //         opacity: 1, scale: 1.0, iconName: 'assets/images/marker.png'),
          //   ));
          // }),
          BlocBuilder<OrderCubit, OrderState>(builder: (context, state) {
            return HereMap(onMapCreated: _onMapCreated);
            // return GoogleMap(
            //     myLocationButtonEnabled: false,
            //     myLocationEnabled: true,
            //     initialCameraPosition: position,
            //     mapType: MapType.normal,
            //     onMapCreated: (GoogleMapController controller) {
            //       _controller.complete(controller);
            //     },
            //     markers: markers.values.toSet());
          }),
          BlocBuilder<OrderCubit, OrderState>(
            builder: (context, state) {
              if (state is OrderStored || state is OrderAcceptedByDriver) {
                final order = (state).order;

                return Container(
                    alignment: Alignment.bottomCenter,
                    margin: EdgeInsets.only(bottom: 60),
                    child: MainButton(
                      splashColor: Colors.white,
                      backgroundColor: Colors.black,
                      borderColor: Colors.black,
                      minWidth: MediaQuery.of(context).size.width - 162,
                      borderWidth: 1,
                      borderRadius: 5,
                      action: () => dialogCancel(context, action: () {
                        BlocProvider.of<OrderCubit>(context).cancelOrder(
                            context: context,
                            order: order,
                            controller: mapController);
                        Navigator.pop(context);
                      }),
                      padding: EdgeInsets.symmetric(vertical: 15),
                      text: Text(
                        !(state is OrderInitial)
                            ? 'Отменить заказ'
                            : 'Вызвать такси',
                        style: GoogleFonts.montserrat(
                            fontSize: 20,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      ),
                    ));
              }
              return Container(
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 60),
                child: MainButton(
                  splashColor: Colors.white,
                  backgroundColor: Colors.black,
                  borderColor: Colors.black,
                  minWidth: MediaQuery.of(context).size.width - 162,
                  borderWidth: 1,
                  borderRadius: 5,
                  action: () => BlocProvider.of<OrderCubit>(context).storeOrder(
                      form: {
                        "point": [latitude.toString(), longitude.toString()]
                            .toString(),
                      },
                      context: context,
                      newLocation: LatLng(latitude, longitude),
                      controller: mapController),
                  padding: EdgeInsets.symmetric(vertical: 15),
                  text: Text(
                    !(state is OrderInitial)
                        ? 'Отменить заказ '
                        : 'Вызвать такси',
                    style: GoogleFonts.montserrat(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                ),
              );
            },
          ),
          BlocBuilder<OrderCubit, OrderState>(
            builder: (context, state) {
              if (state is OrderAcceptedByDriver) {
                final order = state.order;
                int price = 0;
                if (order != null)
                  order.details.forEach((detail) {
                    price += detail['dish']['price'] * detail['quantity'];
                  });
                return Container(
                  margin: EdgeInsets.only(bottom: 150),
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 17, horizontal: 14),
                    width: MediaQuery.of(context).size.width - 170,
                    constraints: BoxConstraints(
                        maxHeight: MediaQuery.of(context).size.height / 4.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.person,
                                size: 20,
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Text(
                                order.driver.user.name,
                                style: GoogleFonts.openSans(
                                    fontSize: 13, fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.emoji_transportation,
                                size: 20,
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Text(
                                order.driver.carId,
                                style: GoogleFonts.openSans(
                                    fontSize: 13, fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.color_lens_rounded,
                                size: 20,
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Text(
                                order.driver.carColor,
                                style: GoogleFonts.openSans(
                                    fontSize: 13, fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.monetization_on_sharp,
                                size: 20,
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Text(
                                order.orderStatus.id == "2"
                                    ? '${price.toString()} тг + ${order.area.deliveryPrice} тг цена доставки'
                                    : order.distance.round() >
                                            order.area.minFreeDistance
                                        ? '${((order.distance.round() * order.area.pricePerKilometer) + order.area.price).round()} тг'
                                        : '${order.area.price.toString()} тг',
                                style: GoogleFonts.openSans(
                                    fontSize: 13, fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.phone,
                                size: 20,
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              GestureDetector(
                                onTap: () => launch(
                                    'tel:${order.driver.user.phone.replaceAll(' ', '')}'),
                                child: Text(
                                  '${order.driver.user.phone}',
                                  style: GoogleFonts.openSans(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w300),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.access_alarm,
                                size: 20,
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Text(
                                '${order.orderStatus.name}',
                                style: GoogleFonts.openSans(
                                    fontSize: 13, fontWeight: FontWeight.w300),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }
              return SizedBox.shrink();
            },
          ),
          Container(
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.only(bottom: 140, right: 20),
            child: ButtonTheme(
              minWidth: 50,
              height: 50,
              child: FlatButton(
                splashColor: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                padding: EdgeInsets.all(8),
                onPressed: () => _currentLocation(),
                child: Icon(
                  Icons.place,
                  color: Colors.white,
                  size: 30,
                ),
                color: Colors.black,
              ),
            ),
          ),
          Builder(
            builder: (context) => Container(
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(top: 100, right: 20),
              child: ButtonTheme(
                minWidth: 50,
                height: 50,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  padding: EdgeInsets.all(8),
                  onPressed: () => Scaffold.of(context).openEndDrawer(),
                  child: Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 30,
                  ),
                  color: Colors.black,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
