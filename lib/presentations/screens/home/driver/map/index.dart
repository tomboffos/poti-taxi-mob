import 'dart:async';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:here_sdk/mapview.dart';
import 'package:location/location.dart';
import 'package:here_sdk/core.dart' as Geo;
import 'package:poti_taxi/components/modal.dart';
import 'package:poti_taxi/cubit/driver/cubit/cancel_reason/cubit/cancel_reason_cubit.dart';
import 'package:poti_taxi/cubit/driver/cubit/driver_cubit.dart';
import 'package:poti_taxi/cubit/driver/cubit/driver_order_cubit.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/cubit/user/order/cubit/order_cubit.dart';
import 'package:poti_taxi/data/models/cancel_reason.dart';
import 'package:poti_taxi/data/repository.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:poti_taxi/service/network_service.dart';
import 'package:url_launcher/url_launcher.dart';

class DriverMap extends StatefulWidget {
  const DriverMap({Key key}) : super(key: key);

  @override
  _DriverMapState createState() => _DriverMapState();
}

class _DriverMapState extends State<DriverMap> {
  CameraPosition position = new CameraPosition(
      target: LatLng(37.43296265331129, -122.08832357078792), zoom: 14.242);
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Completer<GoogleMapController> _controller = Completer();
  Location location = new Location();
  LatLng coordinates;
  dynamic latitude;
  dynamic longitude;
  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  bool search = false;

  getLocation() async {
    LocationData currentLocation;
    currentLocation = await location.getLocation();
    mapController.widgetPins.forEach((e) => e.unpin());
    mapController.camera.lookAtPointWithDistance(
        Geo.GeoCoordinates(currentLocation.latitude, currentLocation.longitude),
        8000);
    double distanceToEarthInMeters = 8000;
    mapController.camera.lookAtPointWithDistance(
        Geo.GeoCoordinates(currentLocation.latitude, currentLocation.longitude),
        distanceToEarthInMeters);
    mapController.pinWidget(
      Image.asset(
        'assets/images/marker.png',
        height: 30,
        width: 30,
      ),
      Geo.GeoCoordinates(currentLocation.latitude, currentLocation.longitude),
    );
  }

  Timer timer;
  double percentage = 0.000000001;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocation();

    timer = new Timer.periodic(Duration(milliseconds: 80), (timer) {
      setState(() {
        percentage += 0.01;
        if (percentage >= 1) {
          percentage = 0;
        }
      });
    });
  }

  dialogCancel(context, {reasons}) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Container(
            height: MediaQuery.of(context).size.height / 2,
            width: MediaQuery.of(context).size.width - 120,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Укажите причину отмены заказа',
                  style: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      color: Color(0xff272727)),
                ),
                SizedBox(
                  height: 20,
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) => Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 25,
                            child: Checkbox(
                                value: false, onChanged: (value) => {}),
                            height: 10,
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Text(
                            '${reasons[index].name}',
                            style: GoogleFonts.montserrat(
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                                color: Color(0xff272727)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 13,
                      ),
                    ],
                  ),
                  itemCount: reasons.length,
                ),
                TextField(
                  maxLines: 5,
                  decoration: InputDecoration(
                      hintText: 'Другое...',
                      hintStyle: GoogleFonts.montserrat(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff272727)),
                      contentPadding: EdgeInsets.all(8),
                      enabledBorder: OutlineInputBorder(
                          gapPadding: 0.0,
                          borderSide: BorderSide(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(5))),
                ),
                Spacer(),
                Center(
                  child: MainButton(
                    splashColor: Colors.white,
                    backgroundColor: Color(0xff272727),
                    borderColor: Color(0xff272727),
                    minWidth: MediaQuery.of(context).size.width - 240,
                    borderWidth: 1,
                    borderRadius: 5,
                    action: () =>
                        Navigator.pushNamed(context, '/register/driver/final'),
                    padding: EdgeInsets.symmetric(vertical: 15),
                    text: Text(
                      'Отправить',
                      style: GoogleFonts.montserrat(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5))),
        );
      },
    );
  }

  HereMapController mapController;

  onMapCreated(HereMapController hereMapController) async {
    setState(() {
      mapController = hereMapController;
    });
    BlocProvider.of<DriverOrderCubit>(context).listenSocketOrders(
        latitude: latitude, longitude: longitude, controller: mapController);
    hereMapController.mapScene.loadSceneForMapScheme(MapScheme.normalDay,
        (MapError error) {
      if (error != null) {
        print('Map scene not loaded. MapError: ${error.toString()}');
        return;
      }
    });

    LocationData currentLocation;
    currentLocation = await location.getLocation();
    mapController.camera.lookAtPointWithDistance(
        Geo.GeoCoordinates(currentLocation.latitude, currentLocation.longitude),
        8000);
    double distanceToEarthInMeters = 8000;
    hereMapController.camera.lookAtPointWithDistance(
        Geo.GeoCoordinates(currentLocation.latitude, currentLocation.longitude),
        distanceToEarthInMeters);
    hereMapController.pinWidget(
      Image.asset(
        'assets/images/marker.png',
        height: 30,
        width: 30,
      ),
      Geo.GeoCoordinates(currentLocation.latitude, currentLocation.longitude),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final orderProvider = BlocProvider.of<DriverOrderCubit>(context);
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      endDrawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              onTap: () => Navigator.pushNamed(context, '/driver/history'),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.history,
                size: 20,
              ),
              title: Text(
                "История заказов",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
            ListTile(
              onTap: () => launch(
                  'https://api.whatsapp.com/send?phone=77017893383&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5'),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.help_outline,
                size: 20,
              ),
              title: Text(
                "Служба поддержки",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
            // ListTile(
            //   minLeadingWidth: 0,
            //   leading: Icon(
            //     Icons.phone_android,
            //     size: 20,
            //   ),
            //   title: Text(
            //     "Сменить номер телефона",
            //     style: GoogleFonts.montserrat(
            //         fontSize: 13,
            //         fontWeight: FontWeight.w400,
            //         color: Color(0xff272727)),
            //   ),
            // ),
            ListTile(
              onTap: () =>
                  BlocProvider.of<UserCubit>(context).logout(context: context),
              minLeadingWidth: 0,
              leading: Icon(
                Icons.logout,
                size: 20,
              ),
              title: Text(
                "Выход",
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          BlocBuilder<DriverOrderCubit, DriverOrderState>(
            builder: (context, state) {
              return HereMap(onMapCreated: onMapCreated);
            },
          ),
          BlocBuilder<DriverOrderCubit, DriverOrderState>(
            builder: (context, state) {
              if (state is OrderAccepted) {
                final order = (state).order;
                return Positioned(
                  bottom: 120,
                  left: 75,
                  right: 75,
                  child: MainButton(
                    splashColor: Colors.white,
                    backgroundColor: Color(0xff272727),
                    borderColor: Color(0xff272727),
                    minWidth: MediaQuery.of(context).size.width - 102,
                    borderWidth: 1,
                    borderRadius: 5,
                    action: () => order.orderStatus.id == 9
                        ? BlocProvider.of<DriverOrderCubit>(context).endOrder(
                            context: context,
                            order: order,
                            controller:mapController
                          )
                        : order.orderStatus.id == 2 && order.orderType.id == 1
                            ? BlocProvider.of<DriverOrderCubit>(context)
                                .stateInPlace(
                                context: context,
                                order: order,
                                controller: mapController
                              )
                            : order.orderStatus.id == 11
                                ? BlocProvider.of<DriverOrderCubit>(context)
                                    .startOrderDriver(
                                        context: context, order: order)
                                : order.orderStatus.id == 12
                                    ? BlocProvider.of<DriverOrderCubit>(context)
                                        .endOrderDriver(
                                            context: context,
                                            order: order,
                                            latitude: latitude.toString(),
                                            longitude: longitude.toString())
                                    : BlocProvider.of<DriverOrderCubit>(context)
                                        .acceptDeliveryOrder(
                                        context: context,
                                        order: order,
                                      ),
                    padding: EdgeInsets.symmetric(vertical: 15),
                    text: Text(
                      order.orderStatus.id == 9 || order.orderStatus.id == 12
                          ? 'Завершить заказ'
                          : order.orderStatus.id == 11
                              ? 'Начать заказ'
                              : order.orderStatus.id == 2 &&
                                      order.orderType.id == 1
                                  ? 'Водитель на месте'
                                  : 'Доставить заказ',
                      style: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ),
                );
              } else if (state is OrderCreated) {
                final order = (state).order;
                return Positioned(
                  bottom: 120,
                  left: 75,
                  right: 75,
                  child: MainButton(
                    splashColor: Colors.white,
                    backgroundColor: Color(0xff272727),
                    borderColor: Color(0xff272727),
                    minWidth: MediaQuery.of(context).size.width - 102,
                    borderWidth: 1,
                    borderRadius: 5,
                    action: () => BlocProvider.of<DriverOrderCubit>(context)
                        .acceptOrder(
                            context: context,
                            order: order,
                            controller: mapController,
                            latitude: latitude.toString(),
                            longitude: longitude.toString()),
                    padding: EdgeInsets.symmetric(vertical: 15),
                    text: Text(
                      'Принять заказ',
                      style: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ),
                );
              }
              return SizedBox.shrink();
            },
          ),
          BlocBuilder<DriverOrderCubit, DriverOrderState>(
            builder: (context, state) {
              if (state is DriverOrderInitial) return SizedBox.shrink();
              final order = state is OrderCreated
                  ? (state).order
                  : (state as OrderAccepted).order;
              return Positioned(
                bottom: 50,
                height: MediaQuery.of(context).size.height * 0.06,
                left: 75,
                right: 75,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  alignment: Alignment.bottomCenter,
                  child: MainButton(
                    splashColor: Colors.white,
                    backgroundColor: Colors.transparent,
                    borderColor: Colors.black,
                    minWidth: MediaQuery.of(context).size.width - 162,
                    borderWidth: 1,
                    borderRadius: 5,
                    action: () => state is OrderAccepted
                        ? {
                            BlocProvider.of<CancelReasonCubit>(context)
                                .getCancelReason(context: context, order: order)
                                .then((cancelReasons) => showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return BlocProvider.value(
                                          value: orderProvider,
                                          child: CancelModal(
                                            mainContext: context,
                                            reasons: cancelReasons,
                                            order: order,
                                            action: () => BlocProvider.of<
                                                    DriverOrderCubit>(context)
                                                .cancelOrder(
                                                    controller: mapController),
                                          ),
                                        );
                                      },
                                    ))
                          }
                        : BlocProvider.of<DriverOrderCubit>(context)
                            .cancelOrder(controller: mapController),
                    padding: EdgeInsets.symmetric(vertical: 15),
                    text: Text(
                      state is OrderAccepted
                          ? 'Отменить заказ'
                          : 'Пропустить заказ',
                      style: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          color: !(state is OrderAccepted)
                              ? Colors.black
                              : Colors.black),
                    ),
                  ),
                ),
              );
            },
          ),
          Container(
            alignment: Alignment.bottomRight,
            margin: EdgeInsets.only(bottom: 140, right: 20),
            child: ButtonTheme(
              minWidth: 50,
              height: 50,
              child: FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                padding: EdgeInsets.all(8),
                onPressed: () => getLocation(),
                child: Icon(
                  Icons.place,
                  color: Colors.white,
                  size: 30,
                ),
                color: Colors.black,
              ),
            ),
          ),
          BlocBuilder<DriverOrderCubit, DriverOrderState>(
            builder: (context, state) {
              if (state is DriverOrderInitial) return SizedBox.shrink();
              if (state is OrderCreated) return SizedBox.shrink();
              final order = (state as OrderAccepted).order;
              int price = 0;
              order.details.forEach((detail) {
                price += detail['dish']['price'] * detail['quantity'];
              });
              return Container(
                margin: EdgeInsets.only(bottom: 200),
                alignment: Alignment.bottomCenter,
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 14),
                  width: MediaQuery.of(context).size.width - 170,
                  constraints: BoxConstraints(
                      maxHeight: order.details != null
                          ? MediaQuery.of(context).size.height / 4.5
                          : MediaQuery.of(context).size.height / 7.5),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      order.details != null
                          ? Container(
                              height: MediaQuery.of(context).size.height / 9,
                              child: ListView.builder(
                                itemBuilder: (context, index) => Container(
                                  child: Center(
                                    child: Text(
                                        '${order.details[index]['dish']['name']} - ${order.details[index]['quantity']} шт'),
                                  ),
                                ),
                                itemCount: order.details.length,
                              ),
                            )
                          : SizedBox.shrink(),
                      order.orderType.id != 2
                          ? SizedBox.shrink()
                          : Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Стоимость заказа - $price Т',
                                    style: GoogleFonts.openSans(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w300),
                                  )
                                ],
                              ),
                            ),
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              order.orderType.id == 2
                                  ? 'Стоимость доставки - ${order.area.deliveryPrice}Т'
                                  : order.distance.round() <
                                          order.area.minFreeDistance
                                      ? 'Стоимость заказа - ${order.area.price}Т'
                                      : 'Стоимость заказа - ${((order.area.pricePerKilometer * order.distance.round()) + order.area.price).round()}Т',
                              style: GoogleFonts.openSans(
                                  fontSize: 13, fontWeight: FontWeight.w300),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () => launch(
                                  'tel:${order.user.phone.replaceAll(' ', '')}'),
                              child: Text(
                                '${order.user.phone}',
                                style: GoogleFonts.openSans(
                                    fontSize: 13, fontWeight: FontWeight.w300),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
          Builder(
            builder: (context) => Container(
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(top: 100, right: 20),
              child: ButtonTheme(
                minWidth: 50,
                height: 50,
                child: FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  padding: EdgeInsets.all(8),
                  onPressed: () => Scaffold.of(context).openEndDrawer(),
                  child: Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 30,
                  ),
                  color: Colors.black,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
