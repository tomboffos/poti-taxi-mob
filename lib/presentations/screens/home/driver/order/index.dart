import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poti_taxi/cubit/driver/cubit/driver_order_cubit.dart';

class OrderHistory extends StatelessWidget {
  const OrderHistory({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<DriverOrderCubit>(context).getOrders();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('История заказов'),
      ),
      body: BlocBuilder<DriverOrderCubit, DriverOrderState>(
        builder: (context, state) {
          if (!(state is DriverOrderFetched))
            return Center(
              child: CircularProgressIndicator(),
            );
          final orders = (state as DriverOrderFetched).orders;
          return ListView.builder(
            itemBuilder: (BuildContext context, index) => Container(
              decoration: BoxDecoration(
                  border: Border.symmetric(
                      horizontal: BorderSide(color: Colors.black))),
              margin: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Заказ номер ${orders[index].id}'),
                      SizedBox(
                        height: 10,
                      ),
                      Text('Пассажир : ${orders[index].user.name}')
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Цена заказа : ${orders[index].area.price} тг'),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            itemCount: orders.length,
          );
        },
      ),
    );
  }
}
