import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/presentations/components/button.dart';

class BlockPage extends StatelessWidget {
  const BlockPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Image.asset(
            'assets/images/logo.png',
            width: 112,
            height: 112,
          )),
          SizedBox(
            height: 20,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40),
            child: Text(
              'Ваш аккаунт был заблокирован! По причине трех отказов. Прошу подождать 24 часа до разблокировки.',
              style: GoogleFonts.montserrat(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff272727).withOpacity(0.5)),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          MainButton(
            action: () => Navigator.pop(context),
            splashColor: Colors.white,
            padding: EdgeInsets.symmetric(vertical: 12),
            minWidth: MediaQuery.of(context).size.width - 102,
            backgroundColor: Colors.black,
            borderColor: Colors.black,
            borderWidth: 2.0,
            borderRadius: 5,
            text: Text(
              'Хорошо',
              style: GoogleFonts.openSans(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
