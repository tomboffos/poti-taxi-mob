import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/driver/cubit/subscription/cubit/subscription_cubit.dart';
import 'package:poti_taxi/presentations/components/button.dart';

class LineChoose extends StatelessWidget {
  const LineChoose({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<SubscriptionCubit>(context).getSubscriptions();
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Image.asset(
            'assets/images/logo.png',
            width: 112,
            height: 112,
          )),
          SizedBox(
            height: 54,
          ),

          SizedBox(
            height: 7,
          ),
          BlocBuilder<SubscriptionCubit, SubscriptionState>(
            builder: (context, state) {
              if (!(state is SubscriptionFetched))
                return Center(
                  child: CircularProgressIndicator(),
                );
              final subscriptions =
                  (state as SubscriptionFetched).subscriptions;
              return ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: subscriptions.length,
                itemBuilder: (_, index) => Container(
                  margin: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
                  child: MainButton(
                    minWidth: MediaQuery.of(context).size.width - 102,
                    backgroundColor:
                        index.isEven ? Colors.transparent : Colors.black,
                    borderColor: Colors.black,
                    borderWidth: 2.0,
                    borderRadius: 5.0,
                    text: Text(
                      subscriptions[index].name,
                      style: GoogleFonts.openSans(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                          color:
                              index.isEven ? Color(0xff272727) : Colors.white),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 12),
                    action: () => BlocProvider.of<SubscriptionCubit>(context)
                        .paySubscription(
                            subscription: subscriptions[index],
                            context: context),
                    // action: () => Navigator.pushNamedAndRemoveUntil(
                    //     context, '/driver/map', (route) => false),
                    splashColor: Colors.black,
                  ),
                ),
              );
            },
          ),
          SizedBox(
            height: 20,
          ),
          // MainButton(
          //   action: () => Navigator.pushNamed(context, '/driver/map'),
          //   splashColor: Colors.white,
          //   padding: EdgeInsets.symmetric(vertical: 12),
          //   minWidth: MediaQuery.of(context).size.width - 102,
          //   backgroundColor: Colors.black,
          //   borderColor: Colors.black,
          //   borderWidth: 2.0,
          //   borderRadius: 5,
          //   text: Text(
          //     '1 месяц',
          //     style: GoogleFonts.openSans(
          //         fontSize: 20,
          //         fontWeight: FontWeight.w600,
          //         color: Colors.white),
          //   ),
          // )
        ],
      ),
    );
  }
}
