import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/presentations/components/button.dart';

class DriverIndex extends StatefulWidget {
  const DriverIndex({Key key}) : super(key: key);

  @override
  _DriverIndexState createState() => _DriverIndexState();
}

class _DriverIndexState extends State<DriverIndex> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Image.asset(
            'assets/images/logo.png',
            width: 112,
            height: 112,
          )),
          SizedBox(
            height: 54,
          ),
          MainButton(
            action: () => Navigator.pushNamed(context, '/driver/choose'),
            splashColor: Colors.white,
            padding: EdgeInsets.symmetric(vertical: 12),
            minWidth: MediaQuery.of(context).size.width - 102,
            backgroundColor: Colors.black,
            borderColor: Colors.black,
            borderWidth: 2.0,
            borderRadius: 5,
            text: Text(
              'Арендовать линию',
              style: GoogleFonts.openSans(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }
}
