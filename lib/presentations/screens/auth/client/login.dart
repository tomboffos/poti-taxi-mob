import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:poti_taxi/presentations/components/form.dart';

class Login extends StatefulWidget {
  final driver;
  Login({Key key, this.driver}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController password = new TextEditingController();
  TextEditingController phone = new TextEditingController();

  bool loading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 100),
              child: Center(
                child: Image.asset(
                  'assets/images/logo.png',
                  width: 112,
                  height: 112,
                ),
              ),
            ),
            MainTextField(
              controller: phone,
              labelText: Text(
                'Введите ваш номер',
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
              hintText: '+7 (___)___-__-__',
              hintStyle: GoogleFonts.montserrat(
                  fontSize: 13,
                  fontWeight: FontWeight.w300,
                  color: Color(0xff272727)),
              margin: EdgeInsets.only(bottom: 20),
            ),
            MainTextField(
              controller: password,
              obscureText: true,
              onSubmit: (value) {
                if (phone.text != '') {
                  BlocProvider.of<UserCubit>(context).login(form: {
                    'phone': phone.text,
                    'password': password.text,
                  }, context: context, driver: widget.driver);
                }
              },
              labelText: Text(
                'Введите пароль',
                style: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727)),
              ),
              hintText: '********',
              hintStyle: GoogleFonts.montserrat(
                  fontSize: 13,
                  fontWeight: FontWeight.w300,
                  color: Color(0xff272727)),
              margin: EdgeInsets.only(bottom: 30),
            ),
            MainButton(
              splashColor: Colors.white,
              backgroundColor: Color(0xff272727),
              borderColor: Color(0xff272727),
              minWidth: MediaQuery.of(context).size.width - 102,
              borderWidth: 1,
              borderRadius: 5,
              action: () {
                setState(() {
                  loading = true;
                });
                BlocProvider.of<UserCubit>(context).login(form: {
                  'phone': phone.text,
                  'password': password.text,
                }, context: context, driver: widget.driver);
              },
              padding: EdgeInsets.symmetric(vertical: 15),
              text: loading
                  ? Center(
                      child: CircularProgressIndicator(color: Colors.white),
                    )
                  : Text(
                      'Войти',
                      style: GoogleFonts.montserrat(
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, '/forget'),
              child: Text(
                'Забыли пароль?',
                style: GoogleFonts.montserrat(fontSize: 15),
              ),
            )
          ],
        ),
      ),
    ));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    setState(() {
      loading = false;
    });
  }
}
