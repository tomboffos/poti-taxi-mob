import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:poti_taxi/presentations/components/form.dart';

class RegisterClient extends StatefulWidget {
  const RegisterClient({Key key}) : super(key: key);

  @override
  _RegisterClientState createState() => _RegisterClientState();
}

class _RegisterClientState extends State<RegisterClient> {
  TextEditingController name = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController passwordConf = new TextEditingController();
  TextEditingController email = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 70, bottom: 15),
                child: Center(
                  child: Image.asset(
                    'assets/images/logo.png',
                    width: 112,
                    height: 112,
                  ),
                ),
              ),
              MainTextField(
                controller: name,
                labelText: Text(
                  'Введите имя',
                  style: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff272727)),
                ),
                hintText: '********',
                hintStyle: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w300,
                    color: Color(0xff272727)),
                margin: EdgeInsets.only(bottom: 20),
              ),
              MainTextField(
                controller: phone,
                labelText: Text(
                  'Введите ваш номер',
                  style: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff272727)),
                ),
                hintText: '+7 (___)___-__-__',
                hintStyle: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w300,
                    color: Color(0xff272727)),
                margin: EdgeInsets.only(bottom: 20),
              ),
              // MainTextField(
              //   controller: email,
              //   obscureText: false,
              //   labelText: Text(
              //     'Введите почту',
              //     style: GoogleFonts.montserrat(
              //         fontSize: 13,
              //         fontWeight: FontWeight.w400,
              //         color: Color(0xff272727)),
              //   ),
              //   hintText: 'your@email.com',
              //   hintStyle: GoogleFonts.montserrat(
              //       fontSize: 13,
              //       fontWeight: FontWeight.w300,
              //       color: Color(0xff272727)),
              //   margin: EdgeInsets.only(bottom: 30),
              // ),
              MainTextField(
                obscureText: true,
                controller: password,
                labelText: Text(
                  'Введите пароль',
                  style: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff272727)),
                ),
                hintText: '********',
                hintStyle: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w300,
                    color: Color(0xff272727)),
                margin: EdgeInsets.only(bottom: 20),
              ),
              MainTextField(
                onSubmit: (string) {
                  name.text != '' &&
                          password.text != '' &&
                          passwordConf.text != '' &&
                          phone.text != ''
                      ? BlocProvider.of<UserCubit>(context).register(form: {
                          'name': name.text,
                          'phone': phone.text,
                          'password': password.text,
                          'password_conf': passwordConf.text
                        }, context: context)
                      : null;
                },
                controller: passwordConf,
                obscureText: true,
                labelText: Text(
                  'Введите пароль',
                  style: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff272727)),
                ),
                hintText: '********',
                hintStyle: GoogleFonts.montserrat(
                    fontSize: 13,
                    fontWeight: FontWeight.w300,
                    color: Color(0xff272727)),
                margin: EdgeInsets.only(bottom: 30),
              ),
              MainButton(
                splashColor: Colors.white,
                backgroundColor: Color(0xff272727),
                borderColor: Color(0xff272727),
                minWidth: MediaQuery.of(context).size.width - 102,
                borderWidth: 1,
                borderRadius: 5,
                action: () =>
                    BlocProvider.of<UserCubit>(context).register(form: {
                  'name': name.text,
                  'phone': phone.text,
                  'password': password.text,
                  'password_conf': passwordConf.text
                }, context: context),
                padding: EdgeInsets.symmetric(vertical: 15),
                text: Text(
                  'Зарегистрироваться',
                  style: GoogleFonts.montserrat(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Colors.white),
                ),
              ),
              GestureDetector(
                onTap: () =>
                    Navigator.pushNamed(context, '/login', arguments: false),
                child: Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Center(
                      child: Text('Войти',
                          style: GoogleFonts.openSans(fontSize: 18))),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
