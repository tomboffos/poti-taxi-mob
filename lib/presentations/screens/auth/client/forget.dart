import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/main/cubit/main_cubit.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:poti_taxi/presentations/components/form.dart';

class ForgetScreen extends StatefulWidget {
  const ForgetScreen({Key key}) : super(key: key);

  @override
  _ForgetScreenState createState() => _ForgetScreenState();
}

class _ForgetScreenState extends State<ForgetScreen> {
  TextEditingController phone = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 40, horizontal: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: 100),
                  child: Center(
                    child: Image.asset(
                      'assets/images/logo.png',
                      width: 112,
                      height: 112,
                    ),
                  ),
                ),
                MainTextField(
                  controller: phone,
                  labelText: Text(
                    'Введите ваш номер',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintText: '+7 (___)___-__-__',
                  hintStyle: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                  margin: EdgeInsets.only(bottom: 20),
                ),
                MainButton(
                  splashColor: Colors.white,
                  backgroundColor: Color(0xff272727),
                  borderColor: Color(0xff272727),
                  minWidth: MediaQuery.of(context).size.width - 102,
                  borderWidth: 1,
                  borderRadius: 5,
                  action: () => BlocProvider.of<MainCubit>(context)
                      .forgetPasswordRequest(
                          context: context, form: {'phone': phone.text}),
                  padding: EdgeInsets.symmetric(vertical: 15),
                  text: Text(
                    'Восстановить пароль',
                    style: GoogleFonts.montserrat(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () => Navigator.pushNamed(context, '/login'),
                  child: Text(
                    'Войти',
                    style: GoogleFonts.montserrat(fontSize: 15),
                  ),
                )
              ]),
        ),
      ),
    );
  }
}
