import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cubit/user_cubit.dart';
import 'package:poti_taxi/presentations/components/button.dart';

class FinalRegistration extends StatefulWidget {
  const FinalRegistration({Key key}) : super(key: key);

  @override
  _FinalRegistrationState createState() => _FinalRegistrationState();
}

class _FinalRegistrationState extends State<FinalRegistration> {
  int index = 0;
  Timer timer;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    timer = new Timer.periodic(Duration(milliseconds: 1200), (timer) {
      setState(() {
        index += 1;
      });
      if (index > 2)
        setState(() {
          index = 0;
        });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 70),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AnimatedContainer(
                      width: index == 0 ? 14 : 10,
                      height: index == 0 ? 14 : 10,
                      decoration: BoxDecoration(
                          color: Color(0xff272727)
                              .withOpacity(index != 0 ? 0.5 : 1),
                          borderRadius: BorderRadius.circular(100)),
                      duration: Duration(milliseconds: 250),
                    ),
                    AnimatedContainer(
                      width: index == 1 ? 14 : 10,
                      height: index == 1 ? 14 : 10,
                      decoration: BoxDecoration(
                          color: Color(0xff272727)
                              .withOpacity(index != 1 ? 0.5 : 1),
                          borderRadius: BorderRadius.circular(100)),
                      duration: Duration(milliseconds: 250),
                    ),
                    AnimatedContainer(
                      width: index == 2 ? 14 : 10,
                      height: index == 2 ? 14 : 10,
                      decoration: BoxDecoration(
                          color: Color(0xff272727)
                              .withOpacity(index != 2 ? 0.5 : 1),
                          borderRadius: BorderRadius.circular(100)),
                      duration: Duration(milliseconds: 250),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 30, top: 11),
                child: Text(
                  'Ваша заявка принята! Прошу вас подождать 24 часа, чтобы вашу заявку обработали!',
                  style: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                  textAlign: TextAlign.center,
                ),
              ),
              MainButton(
                splashColor: Colors.white,
                backgroundColor: Color(0xff272727),
                borderColor: Color(0xff272727),
                minWidth: MediaQuery.of(context).size.width - 102,
                borderWidth: 1,
                borderRadius: 5,
                action: () => BlocProvider.of<UserCubit>(context)
                    .logout(context: context),
                padding: EdgeInsets.symmetric(vertical: 15),
                text: Text(
                  'Хорошо',
                  style: GoogleFonts.montserrat(
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
