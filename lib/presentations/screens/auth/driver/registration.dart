import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:images_picker/images_picker.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:poti_taxi/cubit/driver/cubit/driver_cubit.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:poti_taxi/presentations/components/form.dart';
import 'package:poti_taxi/presentations/components/modal.dart';
import 'package:poti_taxi/presentations/components/upload_images.dart';

class DriverRegistration extends StatefulWidget {
  const DriverRegistration({Key key}) : super(key: key);

  @override
  _DriverRegistrationState createState() => _DriverRegistrationState();
}

class _DriverRegistrationState extends State<DriverRegistration> {
  Media media;
  List<Media> medias;
  TextEditingController name = new TextEditingController();
  TextEditingController surname = new TextEditingController();
  TextEditingController iin = new TextEditingController();
  TextEditingController carModel = new TextEditingController();
  TextEditingController carId = new TextEditingController();
  TextEditingController carColor = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController phone = new TextEditingController();
  var maskFormatter = new MaskTextInputFormatter(
      mask: '############', filter: {"#": RegExp(r'[0-9]')});
  var maskForCarId = new MaskTextInputFormatter(
      mask: '###AAA##', filter: {"#": RegExp(r'[0-9]'), "A": RegExp(r'[A-z]')});

  chooseImage({size, multiple, BuildContext context}) async {
    final res = await ImagesPicker.pick(
        count: size, pickType: PickType.image, language: Language.System);

    if (res != null) {
      setState(() {
        multiple ? medias = res : media = res[0];
      });
      Navigator.pop(context);
    }
  }

  takeImage({size, multiple, BuildContext context}) async {
    final res = await ImagesPicker.openCamera();
    if (res != null)
      setState(() {
        multiple ? medias = res : media = res[0];
      });
    Navigator.pop(context);
  }

  String checkedColor = 'Красный';

  List<dynamic> colors = [
    {'color': Colors.red, 'name': 'Красный'},
    {'color': Colors.blue, 'name': 'Синий'},
    {'color': Colors.black, 'name': 'Черный'},
    {'color': Colors.white, 'name': 'Белый'},
    {'color': Colors.orange, 'name': 'Оранжевый'},
    {'color': Colors.grey, 'name': 'Серый'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 80, horizontal: 24),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: Row(
                    children: [
                      media != null
                          ? GestureDetector(
                              onTap: () => showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                        content: DialogCameraChoose(
                                            firstAction: () => takeImage(
                                                size: 1,
                                                multiple: false,
                                                context: _),
                                            secondAction: () => chooseImage(
                                                size: 1,
                                                multiple: false,
                                                context: _)),
                                      )),
                              child: CircleAvatar(
                                foregroundImage: AssetImage(media.path),
                                radius: 40,
                              ),
                            )
                          : GestureDetector(
                              onTap: () => showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                        content: DialogCameraChoose(
                                            firstAction: () => takeImage(
                                                size: 1,
                                                multiple: false,
                                                context: _),
                                            secondAction: () => chooseImage(
                                                size: 1,
                                                multiple: false,
                                                context: _)),
                                      )),
                              child: Container(
                                padding: EdgeInsets.all(30),
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xff272727)),
                                    borderRadius: BorderRadius.circular(100)),
                                child: Icon(Icons.add),
                              ),
                            ),
                      SizedBox(
                        width: 7,
                      ),
                      Text(
                        'Установите аватар',
                        style: GoogleFonts.montserrat(
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff272727)),
                      )
                    ],
                  ),
                ),
                MainTextField(
                  controller: name,
                  hintText: '********',
                  margin: EdgeInsets.only(bottom: 13),
                  labelText: Text(
                    'Введите имя',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintStyle: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                ),
                MainTextField(
                  controller: surname,
                  hintText: '********',
                  margin: EdgeInsets.only(bottom: 13),
                  labelText: Text(
                    'Введите фамилию',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintStyle: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                ),
                MainTextField(
                  formatters: maskFormatter,
                  controller: iin,
                  margin: EdgeInsets.only(bottom: 13),
                  labelText: Text(
                    'Введите ИИН',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintText: '000000000000',
                  hintStyle: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                ),
                MainTextField(
                  controller: carModel,
                  margin: EdgeInsets.only(bottom: 13),
                  labelText: Text(
                    'Введите модель авто',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintText: 'Аа',
                  hintStyle: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                ),
                MainTextField(
                  controller: phone,
                  margin: EdgeInsets.only(bottom: 13),
                  labelText: Text(
                    'Телефон',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintText: '+7 (___)___-__-__',
                  hintStyle: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                ),
                MainTextField(
                  controller: carId,
                  formatters: maskForCarId,
                  margin: EdgeInsets.only(bottom: 13),
                  labelText: Text(
                    'Введите Гос. номер авто',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintText: 'ХХХаааХХ',
                  hintStyle: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      'Выберите цвет авто',
                      style: GoogleFonts.montserrat(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff272727)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => GestureDetector(
                            onTap: () => setState(
                                () => checkedColor = colors[index]['name']),
                            child: Container(
                              width: 50,
                              height: 50,
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 2,
                                      color:
                                          colors[index]['name'] == checkedColor
                                              ? Colors.black
                                              : Colors.transparent),
                                  color: colors[index]['color'],
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                          ),
                      itemCount: colors.length),
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () => showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                            content: DialogCameraChoose(
                                firstAction: () => takeImage(
                                    size: 3, multiple: true, context: _),
                                secondAction: () => chooseImage(
                                    size: 3, multiple: true, context: _)),
                          )),
                  child: UploadImages(
                    medias: medias,
                    margin: EdgeInsets.only(bottom: 13),
                    text: Text(
                      'Загрузите фото водительских прав',
                      style: GoogleFonts.montserrat(
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff272727)),
                    ),
                  ),
                ),
                MainTextField(
                  onSubmit: (value) => BlocProvider.of<DriverCubit>(context)
                      .registerDriver(form: {
                    "name": name.text,
                    "car_model": carModel.text,
                    "phone": phone.text,
                    "surname": surname.text,
                    "iin": iin.text,
                    "car_color": checkedColor,
                    "car_id": carId.text,
                    "avatar": media != null ? media.path : null,
                    "driver_image": medias != null
                        ? medias.map((e) => e.path).toList()
                        : [],
                    "token": 'device',
                    "password": password.text
                  }, context: context),
                  controller: password,
                  labelText: Text(
                    'Введите пароль',
                    style: GoogleFonts.montserrat(
                        fontSize: 13,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff272727)),
                  ),
                  hintText: '********',
                  hintStyle: GoogleFonts.montserrat(
                      fontSize: 13,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                  margin: EdgeInsets.only(bottom: 20),
                  obscureText: true,
                ),
                // MainTextField(
                //   labelText: Text(
                //     'Введите пароль',
                //     style: GoogleFonts.montserrat(
                //         fontSize: 13,
                //         fontWeight: FontWeight.w400,
                //         color: Color(0xff272727)),
                //   ),
                //   hintText: '********',
                //   hintStyle: GoogleFonts.montserrat(
                //       fontSize: 13,
                //       fontWeight: FontWeight.w300,
                //       color: Color(0xff272727)),
                //   margin: EdgeInsets.only(bottom: 30),
                // ),
                MainButton(
                  splashColor: Colors.white,
                  backgroundColor: Color(0xff272727),
                  borderColor: Color(0xff272727),
                  minWidth: MediaQuery.of(context).size.width - 102,
                  borderWidth: 1,
                  borderRadius: 5,
                  action: () => BlocProvider.of<DriverCubit>(context)
                      .registerDriver(form: {
                    "name": name.text,
                    "car_model": carModel.text,
                    "phone": phone.text,
                    "surname": surname.text,
                    "iin": iin.text,
                    "car_color": checkedColor,
                    "car_id": carId.text,
                    "avatar": media != null ? media.path : null,
                    "driver_image": medias != null
                        ? medias.map((e) => e.path).toList()
                        : [],
                    "token": 'device',
                    "password": password.text
                  }, context: context),
                  padding: EdgeInsets.symmetric(vertical: 15),
                  text: Text(
                    'Зарегистрироваться',
                    style: GoogleFonts.montserrat(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                ),
                GestureDetector(
                  onTap: () =>
                      Navigator.pushNamed(context, '/login', arguments: true),
                  child: Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Center(
                        child: Text('Войти',
                            style: GoogleFonts.openSans(fontSize: 18))),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
