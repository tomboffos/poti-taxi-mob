import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poti_taxi/cubit/main/cubit/main_cubit.dart';

class StartScreen extends StatelessWidget {
  const StartScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<MainCubit>(context).checkToken(context: context);
    return Scaffold(
        backgroundColor: Color(0xff000000),
        body: Center(
          child: Container(
            margin: EdgeInsets.only(left: 20),
            child: Image.asset(
              'assets/images/new3.png',
              height: 250,
              width: 250,
            ),
          ),
        ));
  }
}
