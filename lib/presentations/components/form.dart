import 'package:flutter/material.dart';

class MainTextField extends StatelessWidget {
  final Text labelText;
  final obscureText;
  final controller;
  final String hintText;
  final TextStyle hintStyle;
  final EdgeInsets margin;
  final onSubmit;
  final onChange;
  final formatters;
  const MainTextField(
      {Key key,
      this.labelText,
      this.hintText,
      this.hintStyle,
      this.margin,
      this.controller,
      this.obscureText,
      this.onSubmit,
      this.onChange,
      this.formatters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: labelText,
            margin: EdgeInsets.only(left: 15, bottom: 3),
          ),
          TextField(
            inputFormatters: formatters != null ? [formatters] : [],
            onSubmitted: onSubmit == null ? (value) => {} : onSubmit,
            obscureText: obscureText == null ? false : true,
            onChanged: onChange == null ? (value) => {} : onChange,
            controller: controller,
            decoration: InputDecoration(
              hintText: hintText,
              hintStyle: hintStyle,
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff272727), width: 2),
                  borderRadius: BorderRadius.circular(5)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xff272727), width: 2),
                  borderRadius: BorderRadius.circular(5)),
            ),
          ),
        ],
      ),
    );
  }
}
