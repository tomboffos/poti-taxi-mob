import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/presentations/components/button.dart';

class DialogCameraChoose extends StatelessWidget {
  final firstAction;
  final secondAction;
  const DialogCameraChoose({Key key, this.firstAction, this.secondAction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.width / 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          MainButton(
            splashColor: Colors.white,
            backgroundColor: Color(0xff272727),
            borderColor: Color(0xff272727),
            minWidth: MediaQuery.of(context).size.width - 102,
            borderWidth: 1,
            borderRadius: 5,
            action: firstAction,
            padding: EdgeInsets.symmetric(vertical: 15),
            text: Text(
              'Сделать фото',
              style: GoogleFonts.montserrat(
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Colors.white),
            ),
          ),
          MainButton(
            splashColor: Colors.white,
            backgroundColor: Color(0xff272727),
            borderColor: Color(0xff272727),
            minWidth: MediaQuery.of(context).size.width - 102,
            borderWidth: 1,
            borderRadius: 5,
            action: secondAction,
            padding: EdgeInsets.symmetric(vertical: 15),
            text: Text(
              'Выбрать из галереи',
              style: GoogleFonts.montserrat(
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Colors.white),
            ),
          ),
          MainButton(
            splashColor: Colors.white,
            backgroundColor: Color(0xff272727),
            borderColor: Color(0xff272727),
            minWidth: MediaQuery.of(context).size.width - 102,
            borderWidth: 1,
            borderRadius: 5,
            action: () => Navigator.pop(context),
            padding: EdgeInsets.symmetric(vertical: 15),
            text: Text(
              'Отмениь',
              style: GoogleFonts.montserrat(
                  fontSize: 20,
                  fontWeight: FontWeight.w400,
                  color: Colors.white),
            ),
          )
        ],
      ),
      width: MediaQuery.of(context).size.height / 2,
    );
  }
}
