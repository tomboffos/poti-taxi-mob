import 'package:flutter/material.dart';
import 'package:images_picker/images_picker.dart';

class UploadImages extends StatelessWidget {
  final EdgeInsets margin;
  final Text text;
  final List<Media> medias;

  const UploadImages({Key key, this.margin, this.text, this.medias})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 80,
      margin: margin,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: text,
            margin: EdgeInsets.only(left: 0, bottom: 10),
          ),
          Container(
            height: 50,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) => Container(
                width: 50,
                margin: EdgeInsets.only(right: 12),
                decoration: medias == null
                    ? BoxDecoration(
                        border: Border.all(color: Color(0xff272727), width: 2),
                        borderRadius: BorderRadius.circular(5))
                    : BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(medias[index].path),
                            fit: BoxFit.cover),
                        border: Border.all(color: Color(0xff272727), width: 2),
                        borderRadius: BorderRadius.circular(5)),
                child: index == 0 ? Icon(Icons.add) : SizedBox.shrink(),
              ),
              itemCount: medias == null ? 1 : medias.length,
            ),
          )
        ],
      ),
    );
  }
}
