import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poti_taxi/data/models/organization_category.dart';

class OrganizationType extends StatelessWidget {
  final OrganizationCategory organizationCategory;
  const OrganizationType({Key key, this.organizationCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      child: FlatButton(
          onPressed: () => {},
          padding: EdgeInsets.symmetric(vertical: 6, horizontal: 19),
          child: Text('${organizationCategory.name}'),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
              side: BorderSide(color: Colors.black))),
    );
  }
}
