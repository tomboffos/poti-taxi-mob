import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cart/cubit/cart_cubit.dart';
import 'package:poti_taxi/data/models/dish.dart';
import 'package:poti_taxi/data/models/organization.dart';

class DishItem extends StatefulWidget {
  final Dish dish;
  final Organization organization;
  final List cart;
  DishItem({Key key, this.dish, this.organization, this.cart})
      : super(key: key);

  @override
  _DishItemState createState() => _DishItemState();
}

class _DishItemState extends State<DishItem> {
  int quantity = 0;
  bool tapped = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    quantity =
        widget.cart.where((element) => element['id'] == widget.dish.id).isEmpty
            ? 0
            : widget.cart
                .where((element) => element['id'] == widget.dish.id)
                .toList()[0]['quantity'];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.35,
            height: 113,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: NetworkImage('${widget.dish.image}'),
                  fit: BoxFit.cover),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${widget.dish.name}',
                style: GoogleFonts.montserrat(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                width: 150,
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 300),
                  child: Text(
                    '${widget.dish.description}',
                    style: GoogleFonts.montserrat(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(0.5)),
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ButtonTheme(
                        minWidth: 14,
                        height: 14,
                        child: FlatButton(
                            padding: EdgeInsets.symmetric(
                                vertical: 3, horizontal: 3),
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(3)),
                            onPressed: () {
                              if (quantity > 0) {
                                setState(() {
                                  quantity -= 1;
                                });
                                BlocProvider.of<CartCubit>(context).addToCart(
                                    context: context,
                                    dish: widget.dish,
                                    quantity: quantity,
                                    organization: widget.organization,
                                    minus: true);
                              }
                            },
                            child: Icon(
                              Icons.remove,
                              size: 20,
                            ))),
                    Text('$quantity'),
                    ButtonTheme(
                        minWidth: 14,
                        height: 14,
                        child: FlatButton(
                            padding: EdgeInsets.symmetric(
                                vertical: 3, horizontal: 3),
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(3)),
                            onPressed: () {
                              setState(() {
                                quantity += 1;
                              });
                              BlocProvider.of<CartCubit>(context).addToCart(
                                  context: context,
                                  dish: widget.dish,
                                  quantity: quantity,
                                  organization: widget.organization,
                                  minus: false);
                            },
                            child: Icon(
                              Icons.add,
                              size: 20,
                            ))),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text('${widget.dish.price} тг',
                  style: GoogleFonts.montserrat(
                      fontSize: 12, fontWeight: FontWeight.w400))
            ],
          ),
        ],
      ),
    );
  }
}
