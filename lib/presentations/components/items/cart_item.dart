import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/user/cart/cubit/cart_cubit.dart';
import 'package:poti_taxi/data/models/dish.dart';
import 'package:poti_taxi/data/models/organization.dart';

class CartItem extends StatefulWidget {
  final dynamic dish;
  final Organization organization;
  CartItem({Key key, this.dish, this.organization}) : super(key: key);

  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  int quantity = 1;

  @override
  void initState() {
    super.initState();
    setState(() {
      quantity = widget.dish['quantity'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.35,
            height: 113,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: NetworkImage('${widget.dish['image']}'),
                  fit: BoxFit.cover),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${widget.dish['name']}',
                style: GoogleFonts.montserrat(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                width: 150,
                child: Text(
                  '${widget.dish['description']}',
                  style: GoogleFonts.montserrat(
                      fontSize: 10,
                      fontWeight: FontWeight.bold,
                      color: Colors.black.withOpacity(0.5)),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ButtonTheme(
                        minWidth: 14,
                        height: 14,
                        child: FlatButton(
                            padding: EdgeInsets.symmetric(
                                vertical: 3, horizontal: 3),
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(3)),
                            onPressed: () {
                              if (quantity > 1) {
                                setState(() {
                                  quantity -= 1;
                                });
                              }
                            },
                            child: Icon(
                              Icons.remove,
                              size: 20,
                            ))),
                    Text('$quantity'),
                    ButtonTheme(
                        minWidth: 14,
                        height: 14,
                        child: FlatButton(
                            padding: EdgeInsets.symmetric(
                                vertical: 3, horizontal: 3),
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(3)),
                            onPressed: () {
                              setState(() {
                                quantity += 1;
                              });
                            },
                            child: Icon(
                              Icons.add,
                              size: 20,
                            ))),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text('${widget.dish['price'] * quantity} тг',
                  style: GoogleFonts.montserrat(
                      fontSize: 12, fontWeight: FontWeight.w400))
            ],
          ),
          GestureDetector(
              onTap: () => BlocProvider.of<CartCubit>(context).removeCart(
                  dishId: widget.dish['id'], organization: widget.organization),
              child: Container(
                height: 25,
                child: Icon(
                  Icons.delete,
                  color: Colors.red,
                  size: 25,
                ),
              ))
          // GestureDetector(
          //   onTap: () => BlocProvider.of<CartCubit>(context).addToCart(
          //       context: context,
          //       dish: widget.dish,
          //       quantity: quantity,
          //       organization: widget.organization),
          //   child: Container(
          //     width: 25,
          //     height: 25,
          //     decoration: BoxDecoration(
          //         color: Colors.transparent,
          //         borderRadius: BorderRadius.circular(100),
          //         border: Border.all(color: Colors.black)),
          //   ),
          // )
        ],
      ),
    );
  }
}
