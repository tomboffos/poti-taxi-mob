import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/data/models/dish_category.dart';
import 'package:poti_taxi/data/models/organization.dart';
import 'package:poti_taxi/presentations/components/items/dish.dart';

class DishCategoryItem extends StatefulWidget {
  final DishCategory category;
  final Organization organization;
  final List cart;
  const DishCategoryItem({Key key, this.category, this.organization, this.cart})
      : super(key: key);

  @override
  _DishCategoryItemState createState() => _DishCategoryItemState();
}

class _DishCategoryItemState extends State<DishCategoryItem> {
  bool tapped = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () => setState(() => tapped = !tapped),
            child: Text(
              '${widget.category.name}',
              style: GoogleFonts.montserrat(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff272727).withOpacity(tapped ? 1 : 0.5),
                  decoration:
                      tapped ? TextDecoration.underline : TextDecoration.none),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          tapped
              ? ListView.builder(
                  itemBuilder: (context, index) => DishItem(
                      dish: widget.category.dishes[index],
                      organization: widget.organization,
                      cart: widget.cart),
                  itemCount: widget.category.dishes.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                )
              : SizedBox.shrink(),
        ],
      ),
    );
  }
}
