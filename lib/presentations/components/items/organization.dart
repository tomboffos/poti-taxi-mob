import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/data/models/organization.dart';

class OrganizationItem extends StatelessWidget {
  final Organization organization;
  final String point;
  const OrganizationItem({Key key, this.organization, this.point})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, '/organization',
          arguments: {'organization': organization, 'point': point}),
      child: Container(
        margin: EdgeInsets.only(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 10),
              height: 110,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: NetworkImage('${organization.image}'),
                    fit: BoxFit.cover),
              ),
            ),
            Container(
                margin: EdgeInsets.only(bottom: 3),
                child: Text('${organization.name}',
                    style: GoogleFonts.montserrat(
                        fontSize: 13, fontWeight: FontWeight.w400))),
            Text('${organization.shortDescription}',
                style: GoogleFonts.montserrat(
                    fontSize: 10,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff272727).withOpacity(0.5)))
          ],
        ),
      ),
    );
  }
}
