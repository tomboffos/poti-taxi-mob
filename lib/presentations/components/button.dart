import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final Widget text;
  final Function action;
  final Color splashColor;
  final double minWidth;
  final Color backgroundColor;
  final Color borderColor;
  final EdgeInsets padding;
  final double borderWidth;
  final double borderRadius;

  const MainButton(
      {Key key,
      this.text,
      this.action,
      this.splashColor,
      this.minWidth,
      this.backgroundColor,
      this.borderColor,
      this.padding,
      this.borderWidth,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        minWidth: minWidth,
        splashColor: splashColor,
        child: FlatButton(
          padding: padding,
          shape: RoundedRectangleBorder(
              side: BorderSide(color: borderColor, width: borderWidth),
              borderRadius: BorderRadius.circular(borderRadius)),
          color: backgroundColor,
          onPressed: action,
          child: text,
        ));
  }
}
