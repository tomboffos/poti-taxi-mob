import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:here_sdk/core.dart';
import 'package:poti_taxi/router.dart';
import 'package:poti_taxi/service/notification.dart';

void main() {
  SdkContext.init(IsolateOrigin.main);
  runApp(MyApp(
    router: AppRouter(),
  ));
}

class MyApp extends StatefulWidget {
  final AppRouter router;

  const MyApp({Key key, this.router}) : super(key: key);
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseService().init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            systemOverlayStyle: SystemUiOverlayStyle.light, // 2
            backwardsCompatibility: false, // 1
          )),
      onGenerateRoute: widget.router.generateRouter,
    );
  }
}
