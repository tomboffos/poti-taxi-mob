import 'dart:convert';

import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:adhara_socket_io/manager.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:here_sdk/mapview.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/cubit/driver/cubit/driver_order_cubit.dart';
import 'package:poti_taxi/data/models/directions.dart';
import 'package:poti_taxi/data/models/order.dart';
import 'package:poti_taxi/data/repository.dart';
import 'package:poti_taxi/service/map_service.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart' as YMap;
import 'package:here_sdk/core.dart' as Geo;
part 'order_state.dart';

class OrderCubit extends Cubit<OrderState> {
  final Repository repository;
  OrderCubit({this.repository}) : super(OrderInitial());
  YMap.Polyline polyline;
  Placemark firstPoint;
  Placemark secondPoint;
  void storeOrder(
      {context, form, LatLng newLocation, HereMapController controller}) async {
    repository.processOrder(context: context, form: form).then((order) async {
      if (order != null) {
        controller.camera.lookAtPoint(Geo.GeoCoordinates(
          repository.constructRippleMarkers(location: newLocation).latitude,
          repository.constructRippleMarkers(location: newLocation).longitude,
        ));
        controller.widgetPins.forEach((element) => element.unpin());

        controller.pinWidget(
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
            Geo.GeoCoordinates(
              repository.constructRippleMarkers(location: newLocation).latitude,
              repository
                  .constructRippleMarkers(location: newLocation)
                  .longitude,
            ));

        emit(OrderStored(
            order: order,
            markers: repository.constructRippleMarkers(location: newLocation)));
        trackOrder(order: order, context: context, controller: controller);
      }
    });
  }

  void cancelOrder({context, Order order, HereMapController controller}) async {
    // clear socket instance from manager
    if (polylineHere != null)
      controller.mapScene.removeMapPolyline(polylineHere);
    controller.widgetPins.forEach((e) => e.unpin());
    // dynamic location = new Location();
    // LocationData currentLocation;
    // currentLocation = await location.getLocation();

    repository
        .cancelOrderProcess(context: context, order: order)
        .then((value) => emit(OrderInitial()));
  }

  MapPolyline polylineHere;
  trackOrder({order, context, HereMapController controller}) async {
    print(order.id);
    SocketIO socket;
    socket = await SocketIOManager().createInstance(
        SocketOptions('https://socket.it-lead.net', enableLogging: false));
    socket.onConnect.listen((data) {
      print('connected: $data');
    });
    await socket.connectSync();
    bool cancelled = false;
    socket.on('order-cancelled-by-driver-${order.id}').listen((event) {
      cancelled = true;
      if (context != null)
        repository.networkService.showErrorDialog(
            context: context, message: 'Водитель отклонил заказ');
      controller.mapScene.removeMapPolyline(polylineHere);
      controller.widgetPins.map((e) => e.unpin());
      emit(OrderInited());
    });
    socket
        .on(
      'order-accepted-${order.id}',
    )
        .listen((event) {
      // print('Start point ' + jsonDecode(event[0]['start_point'])[0]);
      if (event[0]['order_type']['id'] == 1) {
        repository
            .getDirectionBeetweenPoints(
                origin: jsonDecode(event[0]['start_point']))
            .then((directions) async {
          controller.widgetPins.forEach((element) => element.unpin());
          // controller.then((value) => value.animateCamera(
          //     CameraUpdate.newLatLngBounds(directions.bounds, 100.0)));
          polylineHere = MapPolyline(
              Geo.GeoPolyline(directions.polylines
                  .map((e) => Geo.GeoCoordinates(e.latitude, e.longitude))
                  .toList()
                  .cast<Geo.GeoCoordinates>()),
              10,
              Colors.black);
          controller.mapScene.addMapPolyline(polylineHere);
          controller.pinWidget(
              Image.asset(
                'assets/images/marker.png',
                height: 30,
                width: 30,
              ),
              Geo.GeoCoordinates(
                jsonDecode(event[0]['start_point'])[0],
                jsonDecode(event[0]['start_point'])[1],
              ));

          // secondPoint = Placemark(
          //   point: Point(
          //       latitude: jsonDecode(event[0]['start_point'])[0],
          //       longitude: jsonDecode(event[0]['start_point'])[1]),
          //   style: PlacemarkStyle(
          //       opacity: 1, scale: 1.0, iconName: 'assets/images/marker.png'),
          // );
          // await controller.addPolyline(polyline);

          emit(OrderAcceptedByDriver(
              order: Order.fromJson(event[0]),
              directions: directions,
              markers: {
                Marker(
                  markerId: MarkerId('origin'),
                  position: LatLng(jsonDecode(event[0]['point'])[0],
                      jsonDecode(event[0]['point'])[1]),
                ),
                Marker(
                  markerId: MarkerId('destination'),
                  position: LatLng(jsonDecode(event[0]['start_point'])[0],
                      jsonDecode(event[0]['start_point'])[1]),
                ),
              }));
        });
      } else {
        repository
            .getDirections(
                origin: jsonDecode(event[0]['start_point']),
                destination: jsonDecode(event[0]['organization']['point']))
            .then((directions) async {
          polylineHere = MapPolyline(
              Geo.GeoPolyline(directions.polylines
                  .map((e) => Geo.GeoCoordinates(e.latitude, e.longitude))
                  .toList()
                  .cast<Geo.GeoCoordinates>()),
              10,
              Colors.black);
          controller.mapScene.addMapPolyline(polylineHere);
          controller.widgetPins.forEach((element) => element.unpin());
          controller.pinWidget(
              Image.asset(
                'assets/images/marker.png',
                height: 30,
                width: 30,
              ),
              Geo.GeoCoordinates(
                jsonDecode(event[0]['start_point'])[0],
                jsonDecode(event[0]['start_point'])[1],
              ));
          controller.pinWidget(
              Image.asset(
                'assets/images/marker.png',
                height: 30,
                width: 30,
              ),
              Geo.GeoCoordinates(
                  jsonDecode(event[0]['organization']['point'])[0],
                  jsonDecode(event[0]['organization']['point'])[1]));
          // controller.addPolyline(polyline);

          // controller.then((value) => value.animateCamera(
          //     CameraUpdate.newLatLngBounds(directions.bounds, 100.0)));

          emit(OrderAcceptedByDriver(
              order: Order.fromJson(event[0]),
              directions: directions,
              markers: {
                Marker(
                  markerId: MarkerId('origin'),
                  position: LatLng(jsonDecode(event[0]['start_point'])[0],
                      jsonDecode(event[0]['start_point'])[1]),
                ),
                Marker(
                  markerId: MarkerId('destination'),
                  position: LatLng(
                      jsonDecode(event[0]['organization']['point'])[0],
                      jsonDecode(event[0]['organization']['point'])[1]),
                ),
              }));
        });
      }
    });
    dynamic echoSubscription;

    socket.on('order-obtained-food-${order.id}').listen((event) {
      print(jsonDecode(event[0]['point']));
      repository
          .getDirections(
              origin: jsonDecode(event[0]['organization']['point']),
              destination: jsonDecode(event[0]['point']))
          .then((directions) async {
        // controller.removePolyline(polyline);
        controller.mapScene.addMapPolyline(polylineHere);
        polylineHere = MapPolyline(
            Geo.GeoPolyline(directions.polylines
                .map((e) => Geo.GeoCoordinates(e.latitude, e.longitude))
                .toList()
                .cast<Geo.GeoCoordinates>()),
            10,
            Colors.black);
        controller.mapScene.addMapPolyline(polylineHere);
        controller.widgetPins.forEach((element) => element.unpin());
        controller.pinWidget(
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
            Geo.GeoCoordinates(jsonDecode(event[0]['point'])[0],
                jsonDecode(event[0]['point'])[1]));
        controller.pinWidget(
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
            Geo.GeoCoordinates(jsonDecode(event[0]['organization']['point'])[0],
                jsonDecode(event[0]['organization']['point'])[1]));
        // await controller.addPolyline(polyline);
        emit(OrderAcceptedByDriver(
            order: Order.fromJson(event[0]),
            directions: directions,
            markers: {
              Marker(
                markerId: MarkerId('origin'),
                position: LatLng(
                    jsonDecode(event[0]['organization']['point'])[0],
                    jsonDecode(event[0]['organization']['point'])[1]),
              ),
              Marker(
                  markerId: MarkerId('destination'),
                  position: LatLng(jsonDecode(event[0]['point'])[0],
                      jsonDecode(event[0]['point'])[1])),
            }));
      });
    });
    socket.on('order-obtained-by-user-${order.id}').listen((event) {
      // controller.removePolyline(polyline);
      controller.widgetPins.forEach((e) => e.unpin());
      controller.mapScene.removeMapPolyline(polylineHere);
      emit(OrderInitial());
    });

    socket.on('order-done-${order.id}').listen((event) {
      cancelled = true;
      // controller.removePolyline(polyline);
      // controller.removePlacemark(firstPoint);
      // controller.removePlacemark(secondPoint);
      controller.widgetPins.forEach((e) => e.unpin());
      controller.mapScene.removeMapPolyline(polylineHere);
      Order caseOrder = Order.fromJson(event[0]);
      repository.networkService.showSuccessMessage(
          context: context, message: '${order.area.price + order.price}Т');
      emit(OrderInitial());
    });
    socket.on('order-cancelled-by-client-${order.id}').listen((event) async {
      cancelled = true;
      // if (polyline != null) controller.removePolyline(polyline);
      // controller.removePlacemark(firstPoint);
      // controller.removePlacemark(secondPoint);
      controller.widgetPins.forEach((e) => e.unpin());
      controller.mapScene.removeMapPolyline(polylineHere);
      await echoSubscription.cancel();
      await SocketIOManager().clearInstance(socket);
      emit(OrderInitial());
    });
    echoSubscription = socket.on('order-started-${order.id}').listen((event) {
      Location location = new Location();
      cancelled = false;
      location.onLocationChanged.listen((LocationData cLoc) {
        // cLoc contains the lat and long of the
        // current user's position in real time,
        // so we're holding on to it
        print(controller.widgetPins);
        controller.widgetPins.forEach((e) => e.unpin());
        controller.mapScene.removeMapPolyline(polylineHere);
        controller.pinWidget(
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
            Geo.GeoCoordinates(cLoc.latitude, cLoc.longitude));
        double distance = MapService().calculateStatic(
            point: jsonDecode(event[0]['point']),
            currentLocation: [cLoc.latitude, cLoc.longitude]);
        Order order = Order.fromJson(event[0]);
        order.distance = distance;
        if (!cancelled)
          emit(OrderAcceptedByDriver(
            order: order,
            markers: null,
            directions: null,
          ));
      });
    });
    // widget.isConsultant ? fetchConsultnantChats() : fetchChats()
  }

  addLocationChangeHandler({point, area}) {}

  void getOrders() {
    repository.processOrders().then((orders) {
      print(orders);
      emit(OrdersFetched(orders: orders));
    });
  }

  void getInit({Future controller}) async {
    emit(OrderInitial());
    Location location = new Location();
    final userLocation = await location.getLocation();
    controller.then((googleController) => googleController.animateCamera(
        CameraUpdate.newLatLngZoom(
            LatLng(userLocation.latitude, userLocation.longitude), 16)));

    emit(OrderInited(
        markers: LatLng(userLocation.latitude, userLocation.longitude)));
  }

  void continueOrder({HereMapController controller, Order order}) {
    emit(OrderStored(
        order: order,
        markers: repository.constructRippleMarkers(
            location: LatLng(
                jsonDecode(order.point)[0], jsonDecode(order.point)[1]))));
    trackOrder(order: order, context: null, controller: controller);
  }
}
