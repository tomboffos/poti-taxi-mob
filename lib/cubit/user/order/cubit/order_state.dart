part of 'order_cubit.dart';

@immutable
abstract class OrderState {
  get order => null;
}

class OrderInitial extends OrderState {
  final markers;

  OrderInitial({this.markers});
}

class OrderStored extends OrderState {
  final Order order;
  final markers;
  OrderStored({this.order, this.markers});
}

class OrderProcessing extends OrderState {}

class OrderAcceptedByDriver extends OrderState {
  final Order order;
  final dynamic markers;
  final Directions directions;

  OrderAcceptedByDriver({this.order, this.markers, this.directions});
}

class OrdersFetched extends OrderState {
  final List<Order> orders;

  OrdersFetched({this.orders});
}

class OrderInited extends OrderState {
  final markers;

  OrderInited({this.markers});
}
