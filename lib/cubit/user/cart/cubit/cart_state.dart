part of 'cart_cubit.dart';

@immutable
abstract class CartState {}

class CartInitial extends CartState {}

class CartFetched extends CartState {
  final List cart;

  CartFetched({this.cart});
}
