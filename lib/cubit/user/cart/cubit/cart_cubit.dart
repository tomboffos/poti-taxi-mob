import 'package:bloc/bloc.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/models/dish.dart';
import 'package:poti_taxi/data/models/organization.dart';
import 'package:poti_taxi/data/repository.dart';

part 'cart_state.dart';

class CartCubit extends Cubit<CartState> {
  final Repository repository;
  CartCubit({this.repository}) : super(CartInitial());

  void addToCart(
      {Dish dish,
      Organization organization,
      int quantity,
      BuildContext context, bool minus}) {
    repository.addDish(
        dish: dish,
        organization: organization,
        quantity: quantity,
        context: context, minus:minus);
  }

  void getCart({Organization organization}) {
    repository
        .getCart(organization: organization)
        .then((value) => emit(CartFetched(cart: value)));
  }

  void removeCart({dishId, Organization organization}) {
    repository
        .removeCart(dishId: dishId, organization: organization)
        .then((value) => emit(CartFetched(cart: value)));
  }

  void storeOrder({form, context, organization}) {
    repository
        .processRestaurantOrder(context: context, form: form)
        .then((value) => getCart(organization: organization));
  }
}
