part of 'dish_category_cubit.dart';

@immutable
abstract class DishCategoryState {}

class DishCategoryInitial extends DishCategoryState {}

class DishCategoryFetched extends DishCategoryState {
  final List<DishCategory> categories;

  DishCategoryFetched({this.categories});
}
