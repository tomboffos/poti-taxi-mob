import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/models/dish_category.dart';
import 'package:poti_taxi/data/repository.dart';

part 'dish_category_state.dart';

class DishCategoryCubit extends Cubit<DishCategoryState> {
  final Repository repository;
  DishCategoryCubit({this.repository}) : super(DishCategoryInitial());

  void getCategories({organizationId}) {
    repository
        .processCategories(organizationId: organizationId)
        .then((value) => emit(DishCategoryFetched(categories: value)));
  }
}
