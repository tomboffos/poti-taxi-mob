part of 'organization_cubit.dart';

@immutable
abstract class OrganizationState {}

class OrganizationInitial extends OrganizationState {}

class OrganizationFetched extends OrganizationState {
  final List<Organization> organizations;

  OrganizationFetched({this.organizations});
}
