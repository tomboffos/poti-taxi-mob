import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:here_sdk/mapview.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/models/organization.dart';
import 'package:poti_taxi/data/repository.dart';
import 'package:here_sdk/core.dart' as Geo;
part 'organization_state.dart';

class OrganizationCubit extends Cubit<OrganizationState> {
  final Repository repository;
  OrganizationCubit({this.repository}) : super(OrganizationInitial());

  void getOrganizations({point,String query}) {
    repository.processOrganizations(point: point, query: query).then((value) {
      emit(OrganizationFetched(organizations: value));
    });
  }

  void getOrganizationsMap({HereMapController controller, point}) {
    repository.processOrganizations(point: point).then((value) {
      value.forEach((element) {
        print(element.logo);
        if (element.logo != '')
          controller.pinWidget(
              Image.network(
                element.logo,
                height: 30,
                width: 30,
              ),
              Geo.GeoCoordinates(
                jsonDecode(element.point)[0],
                jsonDecode(element.point)[1],
              ));
      });
    });
  }
}
