import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/models/user.dart';
import 'package:poti_taxi/data/repository.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final Repository repository;
  UserCubit({this.repository}) : super(UserInitial());

  void register({form, context}) {
    repository.processRegisterUser(form: form, context: context);
  }

  void logout({context}) {
    repository.logout(context: context);
  }

  void getUser() {
    repository.processNewUser().then((value) => emit(UserFetched(user: value)));
  }

  void login({form, context, driver}) {
    repository.processLoginUser(form: form, context: context, driver: driver);
  }

  void updateUser({form, context}) {
    repository
        .updateProcessUser(form: form, context: context)
        .then((value) => getUser());
  }
}
