part of 'user_cubit.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class UserFetched extends UserState {
  final User user;

  UserFetched({this.user});
}
