import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/models/organization_category.dart';
import 'package:poti_taxi/data/repository.dart';

part 'organization_category_state.dart';

class OrganizationCategoryCubit extends Cubit<OrganizationCategoryState> {
  final Repository repository;
  OrganizationCategoryCubit({this.repository})
      : super(OrganizationCategoryInitial());

  void getOrganizationCategory() {
    repository
        .processOrganizationCategory()
        .then((value) => emit(OrganizationCategoryFetched(categories: value)));
  }
}
