part of 'organization_category_cubit.dart';

@immutable
abstract class OrganizationCategoryState {}

class OrganizationCategoryInitial extends OrganizationCategoryState {}

class OrganizationCategoryFetched extends OrganizationCategoryState {
  final List<OrganizationCategory> categories;

  OrganizationCategoryFetched({this.categories});
}
