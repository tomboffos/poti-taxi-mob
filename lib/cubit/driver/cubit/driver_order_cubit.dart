import 'dart:convert';

import 'package:adhara_socket_io/manager.dart';
import 'package:adhara_socket_io/options.dart';
import 'package:adhara_socket_io/socket.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:here_sdk/mapview.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';
import 'package:here_sdk/core.dart' as Geo;
import 'package:poti_taxi/cubit/user/order/cubit/order_cubit.dart';
import 'package:poti_taxi/data/models/directions.dart';
import 'package:poti_taxi/data/models/order.dart';
import 'package:poti_taxi/data/repository.dart';
import 'package:poti_taxi/service/map_service.dart';

part 'driver_order_state.dart';

class DriverOrderCubit extends Cubit<DriverOrderState> {
  final Repository repository;
  DriverOrderCubit({this.repository}) : super(DriverOrderInitial());
  void listenOrders() {
    listenSocketOrders();
  }

  void listenSocketOrders(
      {latitude, longitude, HereMapController controller}) async {
    final user = await repository.processUser();

    SocketIO socket;
    socket = await SocketIOManager().createInstance(
        SocketOptions('https://socket.it-lead.net', enableLogging: false));
    socket.onConnect.listen((data) {
      print('connected: $data');
    });
    await socket.connectSync();
    socket.on('order-created').listen((event) async {
      var location = new Location();
      final userLocation = await location.getLocation();
      latitude = userLocation.latitude;
      longitude = userLocation.longitude;
      print('ORDERCREATEAD');
      repository.processDriverPosition(
          order: Order.fromJson(event[0]),
          latitude: latitude,
          longitude: longitude);
    });
    socket.on('order-sent-request-${user['id']}').listen((event) {
      repository
          .constructMarkerForInitMap(
              order: Order.fromJson(event[0]),
              latitude: latitude,
              longitude: longitude)
          .then((markers) {
        AudioPlayer audioPlayer = AudioPlayer();
        controller.widgetPins.forEach((element) {
          element.unpin();
        });
        controller.pinWidget(
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
            Geo.GeoCoordinates(
              jsonDecode(event[0]['point'])[0],
              jsonDecode(event[0]['point'])[1],
            ));
        controller.pinWidget(
            Image.asset(
              'assets/images/marker.png',
              height: 30,
              width: 30,
            ),
            Geo.GeoCoordinates(
              latitude,
              longitude,
            ));
        audioPlayer.play(
            'https://assets.mixkit.co/sfx/preview/mixkit-alarm-tone-996.mp3');
        emit(OrderCreated(order: Order.fromJson(event[0]), markers: markers));
      });
    });
  }

  void listenCancelSocket(
      {Order order, context, HereMapController controller}) async {
    SocketIO socket;
    socket = await SocketIOManager().createInstance(
        SocketOptions('https://socket.it-lead.net', enableLogging: false));
    await socket.connectSync();

    socket.on('order-cancelled-by-client-${order.id}').listen((event) {
      cancelled = true;
      controller.widgetPins.forEach((element) {
        element.unpin();
      });
      controller.mapScene.removeMapPolyline(polyline);
      repository.networkService.showErrorDialog(
          context: context, message: 'Пассажир отклонил заказ');
      emit(DriverOrderInitial());
    });
  }

  void sendDriverOrderPosition({Order order, latitude, longitude}) {}
  MapPolyline polyline;
  void acceptOrder(
      {context,
      Order order,
      HereMapController controller,
      latitude,
      longitude}) async {
    final newOrder = await repository.acceptOrder(
        context: context,
        order: order,
        latitude: latitude,
        longitude: longitude);
    //     .then((order) {
    //   repository.constructMarkerForInitMap(order: order).then((markers) {
    //     repository
    //         .getDirectionBeetweenPoints(origin: jsonDecode(order.point))
    //         .then((directions) {
    //       controller.then((value) => value.animateCamera(
    //           CameraUpdate.newLatLngBounds(directions.bounds, 100.0)));
    //       emit(OrderAccepted(
    //           order: order, markers: markers, directions: directions));
    //     });
    //   });
    // });
    // final markers =
    //     repository.constructMarkerForInitMapToAccept(order: newOrder);
    final directions = await repository.getDirectionBeetweenPoints(
        origin: newOrder.orderType.id == 2
            ? jsonDecode(newOrder.organization.point)
            : jsonDecode(newOrder.point));

    await listenCancelSocket(
        order: order, context: context, controller: controller);
    polyline = MapPolyline(
        Geo.GeoPolyline(directions.polylines
            .map((e) => Geo.GeoCoordinates(e.latitude, e.longitude))
            .toList()
            .cast<Geo.GeoCoordinates>()),
        10,
        Colors.black);
    controller.mapScene.addMapPolyline(polyline);
    emit(OrderAccepted(order: newOrder, markers: null, directions: directions));
  }

  void acceptDeliveryOrder({context, Order order, Future controller}) async {
    Order newOrder = await repository
        .updateOrder(order: order, form: {'order_status_id': '9'});

    final markers = repository.addMarkers(
        origin: jsonDecode(order.organization.point),
        destination: jsonDecode(order.point));

    final directions = await repository.getDirections(
        origin: jsonDecode(order.organization.point),
        destination: jsonDecode(order.point));
    emit(OrderAccepted(
        order: newOrder, markers: markers, directions: directions));
  }

  void cancelOrder({HereMapController controller}) {
    cancelled = true;
    controller.widgetPins.forEach((element) {
      element.unpin();
    });
    controller.mapScene.removeMapPolyline(polyline);
    emit(DriverOrderInitial());
  }

  void cancelOrderWhileOrdering(
      {Map<String, String> form,
      BuildContext context,
      Order order,
      HereMapController controller}) {
    cancelled = true;
    repository
        .cancelOrderProcessByDriver(context: context, form: form, order: order)
        .then((cancel) {
      emit(DriverOrderInitial());
    });
  }

  void getOrders() {
    repository
        .processDriverOrders()
        .then((orders) => emit(DriverOrderFetched(orders: orders)));
  }

  cancelMainOrder() {
    emit(DriverOrderInitial());
  }

  endOrder({BuildContext context, Order order, HereMapController controller}) {
    controller.widgetPins.forEach((element) {
      element.unpin();
    });
    repository.updateOrder(order: order, form: {'order_status_id': '10'}).then(
        (value) => emit(DriverOrderInitial()));
  }

  stateInPlace(
      {BuildContext context, Order order, HereMapController controller}) {
    repository.updateOrder(order: order, form: {'order_status_id': '11'}).then(
        (value) {
      controller.widgetPins.forEach((element) {
        element.unpin();
      });
      controller.mapScene.removeMapPolyline(polyline);
      controller.pinWidget(
        Image.asset(
          'assets/images/marker.png',
          height: 30,
          width: 30,
        ),
        Geo.GeoCoordinates(
            jsonDecode(order.point)[0], jsonDecode(order.point)[1]),
      );
      emit(OrderAccepted(
          markers: <MarkerId, Marker>{}, order: value, directions: null));
    });
  }

  endOrderDriver(
      {BuildContext context,
      Order order,
      String latitude,
      String longitude}) async {
    repository.updateOrder(order: order, form: {
      'order_status_id': '5',
      'end_point': [latitude.toString(), longitude.toString()].toString()
    }).then((value) {
      print(value.area.price);
      print(value.price);
      repository.networkService.showSuccessMessage(
          context: context,
          message:
              'Стоимость вашего заказа : ${(value.area.price + value.price).toString()}Т');
      emit(DriverOrderInitial());
      cancelled = true;
    });
  }

  bool cancelled = false;
  startOrderDriver({BuildContext context, Order order}) {
    cancelled = false;
    repository.updateOrder(order: order, form: {'order_status_id': '12'}).then(
        (value) {
      Location location = new Location();

      location.onLocationChanged.listen((LocationData cLoc) {
        // cLoc contains the lat and long of the
        // current user's position in real time,
        // so we're holding on to it
        print(cLoc.latitude);
        double distance = MapService().calculateStatic(
            point: jsonDecode(value.point),
            currentLocation: [cLoc.latitude, cLoc.longitude]);
        print(distance);
        print(value);
        value.distance = distance;
        if (!cancelled)
          emit(OrderAccepted(
              order: value, markers: <MarkerId, Marker>{}, directions: null));
      });
    });
  }
}
