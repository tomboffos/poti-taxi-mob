part of 'subscription_cubit.dart';

@immutable
abstract class SubscriptionState {}

class SubscriptionInitial extends SubscriptionState {}

class SubscriptionFetched extends SubscriptionState {
  final List<Subscription> subscriptions;

  SubscriptionFetched({this.subscriptions});
}
