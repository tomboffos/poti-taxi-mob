import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/models/subscription.dart';
import 'package:poti_taxi/data/repository.dart';

part 'subscription_state.dart';

class SubscriptionCubit extends Cubit<SubscriptionState> {
  final Repository repository;
  SubscriptionCubit({this.repository}) : super(SubscriptionInitial());

  void getSubscriptions() {
    repository.processSubscriptions().then((subscriptions) =>
        emit(SubscriptionFetched(subscriptions: subscriptions)));
  }

  void paySubscription({Subscription subscription, context}) {
    repository.processSubscriptionPayment(
        subscription: subscription, context: context);
  }
}
