import 'package:bloc/bloc.dart';
import 'package:images_picker/images_picker.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/repository.dart';

part 'driver_state.dart';

class DriverCubit extends Cubit<DriverState> {
  final Repository repository;
  DriverCubit({this.repository}) : super(DriverInitial());

  void chooseImages({context}) {
    repository
        .pickImages(size: 1)
        .then((value) => emit(DriverAvatarChoosen(media: value[0])));
  }

  void registerDriver({form, context}) {
    repository
        .registerDriver(form: form, context: context)
        .then((driver) => null);
  }
}
