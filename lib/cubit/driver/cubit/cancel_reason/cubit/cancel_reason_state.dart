part of 'cancel_reason_cubit.dart';

@immutable
abstract class CancelReasonState {}

class CancelReasonInitial extends CancelReasonState {}

class CancelReasonFetched extends CancelReasonState {
  final List<CancelReason> cancelReasons;

  CancelReasonFetched({this.cancelReasons});
}
