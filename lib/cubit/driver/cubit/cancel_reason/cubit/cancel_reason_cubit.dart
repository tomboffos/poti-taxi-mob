import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/components/modal.dart';
import 'package:poti_taxi/cubit/user/order/cubit/order_cubit.dart';
import 'package:poti_taxi/data/models/cancel_reason.dart';
import 'package:poti_taxi/data/models/order.dart';
import 'package:poti_taxi/data/repository.dart';
import 'package:poti_taxi/presentations/components/button.dart';

part 'cancel_reason_state.dart';

class CancelReasonCubit extends Cubit<CancelReasonState> {
  final Repository repository;
  CancelReasonCubit({this.repository}) : super(CancelReasonInitial());
  dialogCancel(context, {reasons, Order order}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CancelModal(
          reasons: reasons,
        );
      },
    );
  }

  Future getCancelReason({context, Order order}) async{
    return await repository.processCancelReason();
  }

  void cancelOrder({context, form, Order order}) {
    repository
        .cancelOrderProcessByDriver(context: context, form: form, order: order)
        .then((cancel) => OrderInitial());
  }
}
