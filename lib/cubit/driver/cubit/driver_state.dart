part of 'driver_cubit.dart';

@immutable
abstract class DriverState {}

class DriverInitial extends DriverState {}

class DriverAvatarChoosen extends DriverState {
  final Media media;
  DriverAvatarChoosen({
    this.media,
  });
}
