part of 'driver_order_cubit.dart';

@immutable
abstract class DriverOrderState {}

class DriverOrderInitial extends DriverOrderState {}

class OrderCreated extends DriverOrderState {
  final Order order;
  final Map<MarkerId, Marker> markers;
  OrderCreated({this.order, this.markers});
}

class OrderAccepted extends DriverOrderState {
  final Order order;
  final dynamic markers;
  final Directions directions;
  OrderAccepted({this.order, this.markers, this.directions});
}

class DriverOrderFetched extends DriverOrderState {
  final List<Order> orders;

  DriverOrderFetched({this.orders});
}
