import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:poti_taxi/data/repository.dart';

part 'main_state.dart';

class MainCubit extends Cubit<MainState> {
  final Repository repository;
  MainCubit({this.repository}) : super(MainInitial());

  void checkToken({context}) {
    Timer(
        Duration(seconds: 1),
        () => repository
            .checkUserProcessToken(context: context)
            .then((value) => emit(MainInitial())));
  }

  void forgetPasswordRequest({context, form}) {
    repository.sendRequestForgetPassword(context: context, form: form);
  }
}
