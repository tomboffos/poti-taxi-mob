import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_animarker/core/ripple_marker.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:images_picker/images_picker.dart';
import 'package:location/location.dart';
import 'package:poti_taxi/data/models/directions.dart';
import 'package:poti_taxi/data/models/dish.dart';
import 'package:poti_taxi/data/models/dish_category.dart';
import 'package:poti_taxi/data/models/driver.dart';
import 'package:poti_taxi/data/models/organization_category.dart';
import 'package:poti_taxi/data/models/user_driver.dart';
import 'package:poti_taxi/service/network_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/cancel_reason.dart';
import 'models/order.dart';
import 'models/organization.dart';
import 'models/subscription.dart';
import 'models/user.dart';

class Repository {
  final NetworkService networkService;

  Repository({this.networkService});

  Future processRegisterUser({form, context}) async {
    await networkService.register(form: form, context: context);
  }

  Future checkUserProcessToken({context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token != null)
      await networkService.checkUser(context: context);
    else
      Navigator.pushNamedAndRemoveUntil(context, '/start', (route) => false);
  }

  Future processUser() async {
    return (await networkService.getUser());
  }

  Future processNewUser() async {
    return User.fromJson(await networkService.getUser());
  }

  Future<Order> processOrder({context, form}) async {
    return Order.fromJson(
        await networkService.storeOrder(context: context, form: form));
  }

  Future cancelOrderProcess({context, Order order}) async {
    await networkService.cancelOrderRequest(
      order: order,
      context: context,
    );
  }

  constructRippleMarkers({LatLng location}) {
    Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
    final markerId = MarkerId('order');
    return LatLng(location.latitude, location.longitude);
  }

  constructMarkerForInitMap({Order order, latitude, longitude}) async {
    Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
    final markerId = MarkerId('order');
    final finalMarker = MarkerId('end_point');

    var marker = Marker(
      markerId: markerId,
      position: LatLng(latitude, longitude),
      infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
    );
    if (order.orderType.id != 2) {
      var newMarker = Marker(
        markerId: finalMarker,
        position:
            LatLng(jsonDecode(order.point)[0], jsonDecode(order.point)[1]),
        infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
      );
      markers[finalMarker] = newMarker;
    } else {
      var newMarker = Marker(
        markerId: finalMarker,
        position: LatLng(jsonDecode(order.organization.point)[0],
            jsonDecode(order.organization.point)[1]),
        infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
      );
      markers[finalMarker] = newMarker;
    }
    markers[markerId] = marker;
    return markers;
  }

  constructMarkerForInitMapToAccept({Order order}) {
    Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
    final markerId = MarkerId('order');
    final finalMarker = MarkerId('end_point');

    var marker = Marker(
      markerId: markerId,
      position: LatLng(
          jsonDecode(order.startPoint)[0], jsonDecode(order.startPoint)[1]),
      infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
    );

    if (order.orderType.id != 2) {
      var newMarker = Marker(
        markerId: finalMarker,
        position:
            LatLng(jsonDecode(order.point)[0], jsonDecode(order.point)[1]),
        infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
      );
      markers[finalMarker] = newMarker;
    } else {
      var newMarker = Marker(
        markerId: finalMarker,
        position: LatLng(jsonDecode(order.organization.point)[0],
            jsonDecode(order.organization.point)[1]),
        infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
      );
      markers[finalMarker] = newMarker;
    }
    markers[markerId] = marker;
    return markers;
  }

  void logout({context}) {
    networkService.logout(context: context);
  }

  Future pickImages({context, size}) async {
    List<Media> res = await ImagesPicker.pick(
        count: size, pickType: PickType.image, language: Language.System);
    if (res != null) {
      print(res[0].path);
      return res;
    }
  }

  Future registerDriver({form, context}) async {
    return UserDriver.fromJson(
        await networkService.registerDriver(form: form, context: context));
  }

  Future<List<Subscription>> processSubscriptions() async {
    return (await networkService.fetchSubscriptions())
        .map((subscription) => Subscription.fromJson(subscription))
        .toList();
  }

  Future<dynamic> processSubscriptionPayment(
      {Subscription subscription, context}) async {
    return networkService.fetchPaymentLink(
        subscription: subscription, context: context);
  }

  Future processDriverPosition({Order order, latitude, longitude}) async {
    await networkService.requestDriverPosition(
        order: order, latitude: latitude, longitude: longitude);
  }

  Future<Order> acceptOrder({context, Order order, latitude, longitude}) async {
    return Order.fromJson(await networkService.acceptOrder(
        context: context,
        order: order,
        latitude: latitude,
        longitude: longitude));
  }

  final point =
      'https://maps.googleapis.com/maps/api/directions/json?origin=Disneyland&destination=Universal+Studios+Hollywood&key=YOUR_API_KEY';
  Future<Directions> getDirectionBeetweenPoints({origin}) async {
    Location location = new Location();
    dynamic userLocation = await location.getLocation();
    print(
        'https://maps.googleapis.com/maps/api/directions/json?origin=${userLocation.latitude},${userLocation.longitude}&destination=${origin[0]},${origin[1]}&key=AIzaSyAATKyfY3f3DA9bIn3C5Pn5vwzodKox5zc');
    final response = await get(Uri.parse(
        'https://maps.googleapis.com/maps/api/directions/json?origin=${userLocation.latitude},${userLocation.longitude}&destination=${origin[0]},${origin[1]}&key=AIzaSyAATKyfY3f3DA9bIn3C5Pn5vwzodKox5zc'));
    print('test' +
        PolylinePoints()
            .decodePolyline(jsonDecode(response.body)['routes'][0]
                ['overview_polyline']['points'])
            .toString());
    return Directions.fromJson(jsonDecode(response.body)['routes'][0]);
  }

  Future<List<CancelReason>> processCancelReason() async {
    return (await networkService.fetchCancelReasons())
        .map((cancelReason) => CancelReason.fromJson(cancelReason))
        .toList();
  }

  Future cancelOrderProcessByDriver({context, form, Order order}) async {
    await networkService.fetchCancelByDriver(
        context: context, form: form, order: order);
  }

  Future<List<Order>> processDriverOrders() async {
    return (await networkService.fetchDriverOrders())
        .map((order) => Order.fromJson(order))
        .toList();
  }

  Future processLoginUser({form, context, driver}) async {
    await networkService.loginUser(
        form: form, context: context, driver: driver);
  }

  Future<List<Order>> processOrders() async {
    return (await networkService.fetchOrders())
        .map((orders) => Order.fromJson(orders))
        .toList();
  }

  Future sendRequestForgetPassword({context, form}) async {
    await networkService.sendForgetPassword(context: context, form: form);
  }

  Future<List<Organization>> processOrganizations({point, String query}) async {
    return (await networkService.fetchOrganizations(point: point, query: query))
        .map((organization) => Organization.fromJson(organization))
        .toList();
  }

  Future<List<DishCategory>> processCategories({organizationId}) async {
    return (await networkService.fetchDishCategories(
            organizationId: organizationId))
        .map((dishCategory) => DishCategory.fromJson(dishCategory))
        .toList();
  }

  Future<List<OrganizationCategory>> processOrganizationCategory() async {
    return (await networkService.fetchOrganizationCategory())
        .map((category) => OrganizationCategory.fromJson(category))
        .toList();
  }

  Future addDish(
      {Dish dish,
      Organization organization,
      int quantity,
      BuildContext context,
      bool minus}) async {
    String cart =
        (await SharedPreferences.getInstance()).get('cart_${organization.id}');
    if (cart != null) {
      List<dynamic> decodedCart = jsonDecode(cart);
      if (decodedCart.where((element) => element['id'] == dish.id).isNotEmpty)
        decodedCart.asMap().forEach((index, value) {
          if (value['id'] == dish.id) decodedCart[index]['quantity'] = quantity;
        });
      else
        decodedCart.add({
          'id': dish.id,
          'quantity': quantity,
          'image': dish.image,
          'price': dish.price,
          'description': dish.description,
          'name': dish.name
        });
      print(decodedCart);
      (await SharedPreferences.getInstance())
          .setString('cart_${organization.id}', jsonEncode(decodedCart));
    } else {
      (await SharedPreferences.getInstance()).setString(
          'cart_${organization.id}',
          jsonEncode([
            {
              'id': dish.id,
              'quantity': quantity,
              'image': dish.image,
              'price': dish.price,
              'description': dish.description,
              'name': dish.name
            }
          ]));
    }
    if (minus)
      networkService.showSuccessMessage(
          context: context, message: 'Продукт успешно обновлен');
    else
      networkService.showSuccessMessage(
          context: context, message: 'Продукт добавлен в корзину');
  }

  Future getCart({Organization organization}) async {
    final cart =
        (await SharedPreferences.getInstance()).get('cart_${organization.id}');

    if (cart != null)
      return jsonDecode(cart);
    else
      return [];
  }

  Future removeCart({dishId, Organization organization}) async {
    final cart =
        (await SharedPreferences.getInstance()).get('cart_${organization.id}');

    if (cart != null) {
      List<dynamic> decodedCart = jsonDecode(cart);

      decodedCart.removeWhere((element) => element['id'] == dishId);
      (await SharedPreferences.getInstance())
          .setString('cart_${organization.id}', jsonEncode(decodedCart));
      print(await getCart(organization: organization));
      return (await getCart(organization: organization));
    } else
      return [];
  }

  Future processRestaurantOrder({context, form}) async {
    await networkService.storeRestaurantOrder(context: context, form: form);
  }

  Future updateOrder({Order order, Map<String, String> form}) async {
    return Order.fromJson(
        await networkService.fetchUpdateOrder(order: order, form: form));
  }

  addMarkers({origin, destination}) {
    Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
    final markerId = MarkerId('order');
    final finalMarker = MarkerId('end_point');

    var marker = Marker(
      markerId: markerId,
      position: LatLng(origin[0], origin[1]),
      infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
    );

    var newMarker = Marker(
      markerId: finalMarker,
      position: LatLng(destination[0], destination[1]),
      infoWindow: InfoWindow(title: 'geo_location', snippet: '*'),
    );
    markers[finalMarker] = newMarker;
    markers[markerId] = marker;
    return markers;
  }

  getDirections({origin, destination}) async {
    final response = await get(Uri.parse(
        'https://maps.googleapis.com/maps/api/directions/json?origin=${origin[0]},${origin[1]}&destination=${destination[0]},${destination[1]}&key=AIzaSyAATKyfY3f3DA9bIn3C5Pn5vwzodKox5zc'));
    return Directions.fromJson(jsonDecode(response.body)['routes'][0]);
  }

  Future updateProcessUser({form, context}) async {
    await networkService.updateUser(form: form, context: context);
  }
}
