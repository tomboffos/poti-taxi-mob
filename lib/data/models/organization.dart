class Organization {
  int id;
  String name;
  String image;
  String description;
  String shortDescription;
  String point;
  String logo;

  Organization.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        image = json['image'],
        description = json['description'],
        shortDescription = json['shortDescription'],
        point = json['point'],
        logo = json['logo'];
}
