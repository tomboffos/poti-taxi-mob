import 'package:poti_taxi/data/models/user.dart';

class Driver {
  String iin;
  String carModel;
  String carId;
  String carColor;
  List<String> driverImage;
  String avatar;
  dynamic user;

  Driver.fromJson(Map json)
      : iin = json['iin'],
        carModel = json['car_model'],
        carId = json['car_id'],
        carColor = json['car_color'],
        driverImage = json['driverImage'],
        avatar = json['avatar'],
        user = json['user'] != null ? User.fromJson(json['user']) : null;
}
