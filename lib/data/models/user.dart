class User {
  String name;
  String phone;
  int id;
  bool acceptingOrders;

  User.fromJson(Map json)
      : name = json['name'],
        phone = json['phone'],
        id = json['id'],
        acceptingOrders = json['accepting_orders'];
}
