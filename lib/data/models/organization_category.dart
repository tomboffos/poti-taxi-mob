class OrganizationCategory {
  int id;
  String name;
  int order;

  OrganizationCategory.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        order = json['order'];
}
