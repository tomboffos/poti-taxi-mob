class CancelReason {
  String name;
  int id;

  CancelReason.fromJson(Map json)
      : name = json['name'],
        id = json['id'];
}
