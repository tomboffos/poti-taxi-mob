import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class Directions {
  LatLngBounds bounds;
  List<PointLatLng> polylines;

  Directions.fromJson(Map json)
      : bounds = LatLngBounds(
            southwest: LatLng(json['bounds']['southwest']['lat'],
                json['bounds']['southwest']['lng']),
            northeast: LatLng(json['bounds']['northeast']['lat'],
                json['bounds']['northeast']['lng'])),
        polylines = PolylinePoints()
            .decodePolyline(json['overview_polyline']['points']);
}
