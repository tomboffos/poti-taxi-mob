class OrderType {
  String name;
  int id;

  OrderType.fromJson(Map json)
      : name = json['name'],
        id = json['id'];
}
