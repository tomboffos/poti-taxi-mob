class Subscription {
  String name;
  int duration;
  int price;
  int order;
  int id;

  Subscription.fromJson(Map json)
      : name = json['name'],
        duration = json['duration'],
        price = json['price'],
        order = json['order'],
        id = json['id'];
}
