import 'driver.dart';

class UserDriver {
  String name;
  String surname;
  String phone;
  dynamic driver;

  UserDriver.fromJson(Map json)
      : name = json['name'],
        surname = json['surname'],
        phone = json['phone'],
        driver =
            json['driver'] != null ? Driver.fromJson(json['driver']) : null;
}
