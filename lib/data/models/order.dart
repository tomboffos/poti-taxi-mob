import 'package:poti_taxi/data/models/driver.dart';
import 'package:poti_taxi/data/models/order_status.dart';
import 'package:poti_taxi/data/models/order_type.dart';
import 'package:poti_taxi/data/models/organization.dart';
import 'package:poti_taxi/data/models/user.dart';

import 'area.dart';

class Order {
  dynamic cancelReason;
  dynamic driver;
  dynamic user;
  int id;
  Area area;
  dynamic point;
  dynamic startPoint;
  dynamic distance;
  OrderType orderType;
  dynamic organization;
  dynamic details;
  int price;
  OrderStatus orderStatus;
  Order.fromJson(Map json)
      : user = json['user'] != null ? User.fromJson(json['user']) : null,
        cancelReason = json['cancelReason'],
        driver =
            json['driver'] != null ? Driver.fromJson(json['driver']) : null,
        id = json['id'],
        area = json['area'] != null ? Area.fromJson(json['area']) : null,
        point = json['point'],
        startPoint = json['start_point'],
        orderType = json['order_type'] != null
            ? OrderType.fromJson(json['order_type'])
            : null,
        organization = json['order_type'] != null
            ? json['order_type']['id'] == 2
                ? Organization.fromJson(json['organization'])
                : null
            : null,
        details = json['details'],
        orderStatus = OrderStatus.fromJson(json['order_status']),
        distance = json['distance'],
        price = json['price'] != null ? json['price'] : 0;
}
