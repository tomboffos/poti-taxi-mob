import 'package:poti_taxi/data/models/dish.dart';

class DishCategory {
  int id;
  String name;
  List<dynamic> dishes;

  DishCategory.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        dishes = json['dishes'].map((dish) => Dish.fromJsom(dish)).toList();
}
