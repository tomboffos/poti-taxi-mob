class OrderStatus {
  String name;
  int id;

  OrderStatus.fromJson(Map json)
      : name = json['name'],
        id = json['id'];
}
