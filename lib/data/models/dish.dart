class Dish {
  String name;
  String image;
  String description;
  int price;
  int id;

  Dish.fromJsom(Map json)
      : name = json['name'],
        image = json['image'],
        description = json['description'],
        price = json['price'],
        id = json['id'];
}
