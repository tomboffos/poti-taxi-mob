class Area {
  int price;
  int deliveryPrice;
  int minFreeDistance;
  int nightPrice;
  int pricePerKilometer;
  Area.fromJson(Map json)
      : price = json['price'] != null ? json['price'] : null,
        deliveryPrice = json['delivery_price'],
        minFreeDistance = json['min_free_distance'],
        pricePerKilometer = json['price_per_kilometer'];
}
