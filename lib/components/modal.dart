import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:poti_taxi/cubit/driver/cubit/cancel_reason/cubit/cancel_reason_cubit.dart';
import 'package:poti_taxi/cubit/driver/cubit/driver_order_cubit.dart';
import 'package:poti_taxi/data/models/cancel_reason.dart';
import 'package:poti_taxi/data/models/order.dart';
import 'package:poti_taxi/presentations/components/button.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class CancelModal extends StatefulWidget {
  final List<CancelReason> reasons;
  final action;
  final Order order;
  final controller;
  final BuildContext mainContext;
  CancelModal(
      {Key key,
      this.reasons,
      this.order,
      this.mainContext,
      this.action,
      this.controller})
      : super(key: key);

  @override
  _CancelModalState createState() => _CancelModalState();
}

class _CancelModalState extends State<CancelModal> {
  int reasonId;
  TextEditingController cancelComment = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: MediaQuery.of(context).size.height / 2,
        width: MediaQuery.of(context).size.width - 120,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Укажите причину отмены заказа',
              style: GoogleFonts.montserrat(
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff272727)),
            ),
            SizedBox(
              height: 20,
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) => Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 25,
                        child: Checkbox(
                          value: reasonId == widget.reasons[index].id,
                          onChanged: (value) => setState(
                              () => reasonId = widget.reasons[index].id),
                        ),
                        height: 10,
                      ),
                      SizedBox(
                        width: 13,
                      ),
                      Text(
                        '${widget.reasons[index].name}',
                        style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                            color: Color(0xff272727)),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 13,
                  ),
                ],
              ),
              itemCount: widget.reasons.length,
            ),
            TextField(
              maxLines: 5,
              controller: cancelComment,
              decoration: InputDecoration(
                  hintText: 'Другое...',
                  hintStyle: GoogleFonts.montserrat(
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff272727)),
                  contentPadding: EdgeInsets.all(8),
                  enabledBorder: OutlineInputBorder(
                      gapPadding: 0.0,
                      borderSide: BorderSide(
                        color: Colors.black,
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(5))),
            ),
            Spacer(),
            Center(
              child: MainButton(
                splashColor: Colors.white,
                backgroundColor: Color(0xff272727),
                borderColor: Color(0xff272727),
                minWidth: MediaQuery.of(context).size.width - 240,
                borderWidth: 1,
                borderRadius: 5,
                action: () {
                  BlocProvider.of<DriverOrderCubit>(context)
                      .cancelOrderWhileOrdering(
                    form: {
                      'order_status_id': '3',
                      'cancel_reason_id': reasonId.toString(),
                      'cancel_comment': cancelComment.text
                    },
                    context: widget.mainContext,
                    order: widget.order,
                  );
                  Navigator.pop(context);
                },
                padding: EdgeInsets.symmetric(vertical: 15),
                text: Text(
                  'Отправить',
                  style: GoogleFonts.montserrat(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5))),
    );
  }
}
