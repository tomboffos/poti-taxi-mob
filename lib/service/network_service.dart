import 'dart:convert';
import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:poti_taxi/data/models/dish_category.dart';
import 'package:poti_taxi/data/models/driver.dart';
import 'package:poti_taxi/data/models/order.dart';
import 'package:poti_taxi/data/models/subscription.dart';
import 'package:poti_taxi/data/models/user.dart';
import 'package:poti_taxi/data/models/user_driver.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NetworkService {
  final apiEndpoint = 'https://poti-taxi.it-lead.net/api';
  Future register({form, context}) async {
    if (form['password'] != form['password_conf']) {
      showErrorDialog(context: context, message: 'Пароли не совпадают');
      return null;
    }
    final device = (await SharedPreferences.getInstance()).get('device_token');
    form['token'] = device;
    final response = await post(Uri.parse('$apiEndpoint/auth/user/register'),
        body: form, headers: {"Accept": "application/json"});
    print(response.body);
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }

    if (response.statusCode == 201) {
      (await SharedPreferences.getInstance())
          .setString('token', jsonDecode(response.body)['token']);
      final location = new Location();
      LocationData userData = await location.getLocation();
      Navigator.pushNamed(context, '/home/index',
          arguments: {'location': userData});
      // return jsonDecode(response.body)['user'] as Map;
    }
  }

  Future checkUser({context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(Uri.parse('$apiEndpoint/user'), headers: {
      "Authorization": "Bearer $token",
      'Accept': 'application/json'
    });
    print(response.body);
    if (response.statusCode == 401) {
      Navigator.pushNamedAndRemoveUntil(context, '/start', (route) => false);
    }
    bool _serviceEnabled;

    final location = new Location();
    LocationData userData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    PermissionStatus _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.granted) {
      _permissionGranted = await location.requestPermission();
      await location.requestService();

      userData = await location.getLocation();
    }
    if (_permissionGranted == PermissionStatus.denied) {
      userData =
          LocationData.fromMap({'latitude': 43.230707, 'longitude': 76.890273});
    }

    if (jsonDecode(response.body)['data']['role_id'] == 1)
      Navigator.pushNamedAndRemoveUntil(
          context, '/home/index', (route) => false,
          arguments: {'location': userData});
    else if (jsonDecode(response.body)['data']['role_id'] == 2 &&
        jsonDecode(response.body)['data']['accepted'] == true &&
        jsonDecode(response.body)['data']['blocked'] == true) {
      if ((await SharedPreferences.getInstance()).getBool('user')) {
        Navigator.pushNamedAndRemoveUntil(
          context,
          '/driver/choose',
          (route) => false,
        );
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map', (route) => false,
            arguments: {'location': userData});
      }
    } else if ((jsonDecode(response.body)['data']['role_id'] == 2 &&
            jsonDecode(response.body)['data']['accepted'] == false &&
            jsonDecode(response.body)['data']['blocked'] == false) ||
        (jsonDecode(response.body)['data']['accepted'] == false &&
            jsonDecode(response.body)['data']['blocked'] == true)) {
      if ((await SharedPreferences.getInstance()).getBool('user')) {
        Navigator.pushNamedAndRemoveUntil(
          context,
          '/home/index',
          (route) => false,
          arguments: {'location': userData},
        );
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, '/register/driver/final', (route) => false);
      }
    } else if (jsonDecode(response.body)['data']['role_id'] == 2 &&
        jsonDecode(response.body)['data']['accepted'] == true &&
        jsonDecode(response.body)['data']['blocked'] == false) {
      if ((await SharedPreferences.getInstance()).getBool('user')) {
        Navigator.pushNamedAndRemoveUntil(
            context, '/home/index', (route) => false,
            arguments: {'location': userData});
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map', (route) => false,
            arguments: {'location': userData});
      }
    }
  }

  Future getUser() async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(Uri.parse('$apiEndpoint/user'), headers: {
      "Authorization": "Bearer $token",
      'Accept': "application/json"
    });
    print(response.body);

    return jsonDecode(response.body)['data'];
  }

  Future<dynamic> storeOrder({context, form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    print(token);
    final response = await post(Uri.parse('$apiEndpoint/user/orders'),
        body: form,
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        });
    if (response.statusCode == 400)
      return showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);
    if (response.statusCode == 201) return jsonDecode(response.body)['data'];
  }

  Future<dynamic> cancelOrderRequest({context, Order order}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    print(order.id);
    final response = await patch(
        Uri.parse('$apiEndpoint/user/orders/${order.id}'),
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        },
        body: {
          "order_status_id": "4"
        });
    print(response.statusCode);
    print(response.body);
    return null;
  }

  showSuccessMessage({context, message}) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.NO_HEADER,
        dialogBackgroundColor: Colors.green,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Успешно',
        desc: '$message',
        autoHide: Duration(milliseconds: 3000))
      ..show();
  }

  showErrorDialog({context, message}) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.NO_HEADER,
        animType: AnimType.BOTTOMSLIDE,
        dialogBackgroundColor: Colors.red,
        title: 'Ошибка',
        desc: '$message',
        autoHide: Duration(milliseconds: 1500))
      ..show();
  }

  void logout({context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    print(token);
    final response = await post(Uri.parse('$apiEndpoint/user/logout'),
        headers: {"Authorization": "Bearer $token"});
    print(response.body);
    if (response.statusCode == 200) {
      (await SharedPreferences.getInstance()).remove('token');
      Navigator.pushNamedAndRemoveUntil(context, '/start', (route) => false);
    } else
      showErrorDialog(
          context: context, message: 'Ошибка попробуйте повторить позже');
  }

  Future<Map<String, dynamic>> registerDriver({form, context}) async {
    // final response = await post(Uri.parse('$apiEndpoint/auth/driver/register'),
    //     headers: {
    //       "Accept": "application/json",
    //       // "Content-Type": "multipart/form-data"
    //     },
    //     body: form);
    final device = (await SharedPreferences.getInstance()).get('device_token');
    form['token'] = device;
    final request = MultipartRequest(
        'POST', Uri.parse('$apiEndpoint/auth/driver/register'));
    if (form['avatar'] != null) {
      request.files.add(
        MultipartFile('avatar', File(form['avatar']).readAsBytes().asStream(),
            File(form['avatar']).lengthSync(),
            filename: '${DateTime.now().toString()}-avatar'),
      );
    }
    form['driver_image'].forEach((image) {
      request.files.add(MultipartFile('driver_image[]',
          File(image).readAsBytes().asStream(), File(image).lengthSync(),
          filename: '${DateTime.now().toString()}-driver_image'));
    });
    request.fields.addAll({
      "name": form['name'],
      "car_model": form['car_model'],
      "phone": form['phone'],
      "surname": form['surname'],
      "iin": form['iin'],
      "car_color": form['car_color'],
      "car_id": form['car_id'],
      "token": device,
      "password": form['password'],
    });
    request.headers.addAll({
      'Accept': 'application/json',
    });
    final response = await request.send();
    final body = await response.stream.bytesToString();
    // print(request.);
    print(jsonDecode(body));
    if (response.statusCode == 201) {
      (await SharedPreferences.getInstance())
          .setString('token', jsonDecode(body)['token']);
      Navigator.pushNamedAndRemoveUntil(
          context, '/register/driver/final', (route) => false);
    }

    print(body);
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }
  }

  Future<List<dynamic>> fetchSubscriptions() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndpoint/driver/subscription'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });

    return jsonDecode(response.body)['data'] as List;
  }

  Future<dynamic> fetchPaymentLink({Subscription subscription, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(
        Uri.parse('$apiEndpoint/driver/driver-subscription'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        },
        body: {
          'subscription_id': subscription.id.toString(),
        });
    print(jsonDecode(response.body));
    print(response.statusCode);
    if (response.statusCode == 200)
      Navigator.pushNamed(context, '/payment',
          arguments: jsonDecode(response.body)['payment']['pg_redirect_url']);
  }

  Future requestDriverPosition({Order order, latitude, longitude}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    print('TOKEN ' + token);
    final response =
        await post(Uri.parse('$apiEndpoint/driver/order-driver'), headers: {
      'Authorization': 'Bearer $token',
      'Accept': 'application/json'
    }, body: {
      'order_id': order.id.toString(),
      'location': [latitude.toString(), longitude.toString()].toString()
    });

    print(response.body);
    print(response.statusCode);
  }

  Future<dynamic> acceptOrder(
      {context, Order order, latitude, longitude}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response =
        (await put(Uri.parse('$apiEndpoint/driver/orders/${order.id}'), body: {
      'order_status_id': 2.toString(),
      'accept': true.toString(),
      'start_point': [latitude.toString(), longitude.toString()].toString()
    }, headers: {
      "Authorization": 'Bearer $token',
      'Accept': 'application/json'
    }));
    print(response.body);
    return jsonDecode(response.body)['data'];
  }

  Future<List<dynamic>> fetchCancelReasons() async {
    final response = await get(Uri.parse('$apiEndpoint/cancel-reason'),
        headers: {'Accept': "application/json"});

    return jsonDecode(response.body);
  }

  Future fetchCancelByDriver({context, Order order, form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await put(
        Uri.parse('$apiEndpoint/driver/orders/${order.id}'),
        body: form,
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });
    print(response.statusCode);
    // showSuccessMessage(context: context, message: 'Заказ отменен');
  }

  Future<List<dynamic>> fetchDriverOrders() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndpoint/driver/orders'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });
    print(jsonDecode(response.body));
    return jsonDecode(response.body)['data'];
  }

  loginUser({form, context, driver}) async {
    final device = (await SharedPreferences.getInstance()).get('device_token');
    form['token'] = device;
    final response = await post(Uri.parse('$apiEndpoint/auth/driver/login'),
        body: form, headers: {'Accept': 'application/json'});

    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }

    if (response.statusCode == 200) {
      print(jsonDecode(response.body)['user']);

      final location = new Location();

      bool _serviceEnabled;
      LocationData userData;
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }
      PermissionStatus _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.granted) {
        _permissionGranted = await location.requestPermission();
        await location.requestService();

        userData = await location.getLocation();
      }
      if (_permissionGranted == PermissionStatus.denied) {
        userData = LocationData.fromMap(
            {'latitude': 43.230707, 'longitude': 76.890273});
      }

      (await SharedPreferences.getInstance())
          .setString('token', jsonDecode(response.body)['token']);
      final UserDriver user =
          UserDriver.fromJson(jsonDecode(response.body)['user']);
      if ((jsonDecode(response.body)['user']['role_id'] == 1 &&
              driver == false) ||
          (jsonDecode(response.body)['user']['role_id'] == 2 &&
              driver == false)) {
        (await SharedPreferences.getInstance()).setBool('user', true);
        Navigator.pushNamedAndRemoveUntil(
            context, '/home/index', (route) => false,
            arguments: {'location': userData});
      } else if (user.driver == null)
        showErrorDialog(
            context: context, message: 'Не правильный логин или пароль');
      else if (jsonDecode(response.body)['user']['role_id'] == 2 &&
          jsonDecode(response.body)['user']['driver']['accepted'] == true &&
          jsonDecode(response.body)['user']['driver']['accepted'] == true) {
        (await SharedPreferences.getInstance()).setBool('user', false);
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map', (route) => false,
            arguments: {'location': userData});
      } else if (jsonDecode(response.body)['user']['role_id'] == 2 &&
          jsonDecode(response.body)['user']['driver']['accepted'] == false &&
          jsonDecode(response.body)['user']['driver']['blocked'] == true) {
        (await SharedPreferences.getInstance()).setBool('user', false);
        Navigator.pushNamedAndRemoveUntil(
            context, '/register/driver/final', (route) => false);
      } else if (jsonDecode(response.body)['user']['role_id'] == 2 &&
          jsonDecode(response.body)['user']['driver']['accepted'] == false &&
          jsonDecode(response.body)['user']['driver']['blocked'] == false) {
        (await SharedPreferences.getInstance()).setBool('user', false);
        Navigator.pushNamedAndRemoveUntil(
            context, '/register/driver/final', (route) => false);
      } else if (jsonDecode(response.body)['user']['role_id'] == 2 &&
          jsonDecode(response.body)['user']['driver']['accepted'] == true &&
          jsonDecode(response.body)['user']['driver']['blocked'] == false) {
        (await SharedPreferences.getInstance()).setBool('user', false);
        Navigator.pushNamedAndRemoveUntil(
            context, '/driver/map', (route) => false,
            arguments: {'location': userData});
      }
    }

    if (response.statusCode == 400) {
      showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);
    }
  }

  Future<List<dynamic>> fetchOrders() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndpoint/user/orders'),
        headers: {'Authorization': 'Bearer $token'});
    print(response.body);
    return jsonDecode(response.body)['data'] as List;
  }

  sendForgetPassword({context, form}) async {
    final response = await post(Uri.parse('$apiEndpoint/auth/forget'),
        body: form, headers: {'Accept': 'application/json'});
    print(form);
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }
    if (response.statusCode == 200)
      showSuccessMessage(
          context: context,
          message: 'На вашу почту был прислан измененный пароль ');
  }

  Future<List<dynamic>> fetchOrganizations({point, String query}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    print(point);
    if (point == '[null,null]') {
      Position position = await Geolocator.getCurrentPosition();

      point = [position.latitude, position.longitude].toString();
      final response = await get(
          Uri.parse(
              '$apiEndpoint/user/organizations?point=$point${query != null ? '&search=$query' : ''}'),
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $token'
          });
      if (response.statusCode == 400) return [];
      return jsonDecode(response.body)['data'] as List;
    }
    final response = await get(
        Uri.parse(
            '$apiEndpoint/user/organizations?point=$point${query != null ? '&search=$query' : ''}'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token'
        });
    if (response.statusCode == 400) return [];
    print(response.body);

    return jsonDecode(response.body)['data'] as List;
  }

  Future<List<dynamic>> fetchDishCategories({organizationId}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse('$apiEndpoint/user/organizations/$organizationId'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });
    print(jsonDecode(response.body));
    return jsonDecode(response.body)['data'] as List;
  }

  Future<List<dynamic>> fetchOrganizationCategory() async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse('$apiEndpoint/user/organization-category'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token'
        });
    return jsonDecode(response.body) as List;
  }

  storeRestaurantOrder({context, form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    form['basket'] = (await SharedPreferences.getInstance())
        .get('cart_${form['organization_id']}');
    print(form['basket']);
    if (form['basket'] == null || jsonDecode(form['basket']).length == 0)
      return showErrorDialog(
          context: context, message: 'К сожалению ваша корзина пуста');
    final response =
        await post(Uri.parse('$apiEndpoint/user/restaurants/orders'),
            headers: {
              'Authorization': 'Bearer $token',
              'Accept': 'application/json',
            },
            body: form);
    print(form);
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }
    if (response.statusCode == 201) {
      (await SharedPreferences.getInstance())
          .remove('cart_${form['organization_id']}');
      showSuccessMessage(context: context, message: 'Заказ сохранен');
      print(jsonDecode(response.body));
      final location = new Location();
      final locationData = await location.getLocation();
      Navigator.pushNamedAndRemoveUntil(
          context, '/home/index', (route) => false,
          arguments: {
            'order': Order.fromJson(
              jsonDecode(response.body)['data'],
            ),
            'location': locationData
          });
    }
  }

  fetchUpdateOrder({Order order, Map<String, String> form}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    if (form['order_status_id'] == '5') {
      Location location = new Location();
      dynamic userLocation = await location.getLocation();
      form['end_point'] = [
        userLocation.latitude.toString(),
        userLocation.longitude.toString()
      ].toString();
    }
    final response = await put(
        Uri.parse('$apiEndpoint/driver/orders/${order.id}'),
        body: form,
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });
    print(response.body);
    return jsonDecode(response.body)['data'];
  }

  Future updateUser({form, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await post(Uri.parse('$apiEndpoint/user'),
        body: form,
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });

    showSuccessMessage(
        context: context, message: 'Успешно обновлен пользователь');
  }
}
