import 'dart:math';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapService {
  double calculateStatic({point,List currentLocation}) {
    var latFrom = deg2rad(point[0]);
    var lonFrom = deg2rad(point[1]);
    var latTo = deg2rad(currentLocation[0]);
    var lonTo = deg2rad(currentLocation[1]);

    var lonDelta = lonTo - lonFrom;
    var a = pow(cos(latTo) * sin(lonDelta), 2) +
        pow(
            cos(latFrom) * sin(latTo) -
                sin(latFrom) * cos(latTo) * cos(lonDelta),
            2);
    var b =
        sin(latFrom) * sin(latTo) + cos(latFrom) * cos(latTo) * cos(lonDelta);

    var angle = atan2(sqrt(a), b);
    return angle * 6371000;
  }

  deg2rad(point) {
    return (point * pi / 180.0);
  }
}
