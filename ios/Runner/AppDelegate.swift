import UIKit
import Flutter
import GoogleMaps
import YandexMapsMobile



@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyBmPqTMeQmCG41T631s_sKngjIQrfapxWI")
    YMKMapKit.setApiKey("a2a57c80-4e44-40ae-9dcb-ad2241c3acd1")
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
